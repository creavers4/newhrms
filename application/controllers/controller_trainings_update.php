<?php
	class Controller_trainings_update extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){

			$query = $this->model_training->trainings_view_for_update();
			if($query){
				$data['trainings_update'] = $query;
			}
			$this->load->view('training/view_trainings_update', $data);

		}

		public function view_trainings(){
			$select = set_value('select_training');
			$query1 = $this->model_training->trainings_view_for_select($select);
			if($query1){
				$data['trainings_view'] = $query1;
			}
			$this->load->view('training/view_trainings_update', $data);
		}
		public function update_trainings(){
			$select = set_value('select_training');
			$update = array('specify_schedule' 			=> set_value('specify_schedule'),
							'fee' 						=> set_value('fee'));

			$this->model_training->update_trainings($update,$select);
			redirect('controller_trainings_update');
		}
		}
		

?>