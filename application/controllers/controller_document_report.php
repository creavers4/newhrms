<?php
class Controller_document_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $document_report = $this->model_report->document_report();
			if($document_report){
				$data['document'] = $document_report;
			}
        $this->load->view("report/view_document_report",$data);
    }
}
?>