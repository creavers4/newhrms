<?php
	class Controller_trainees_drop extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$data['training_listb'] = $this->model_training->trainings_drop_down();
		
		$this->load->view('training/view_training', $data);
		}
	}
?>