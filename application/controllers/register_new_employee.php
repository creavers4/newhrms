<?php
 class Register_new_employee extends CI_Controller{

     public function __construct ()
	{
		parent::__construct();
		if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
		$this->load->model('emp_info');
	}
    
    public function index(){
       $this->load->view('employee/register_new_employee');

    }
    public function create(){
		
		$this->form_validation->set_rules('fname','First Name','required|alpha_numeric|min_length[2]|max_length[20]');
       $this->form_validation->set_rules('lname','Last Name','required|alpha_numeric|min_length[2]|max_length[20]');	
		if ($this->form_validation->run() == FALSE)
		{ 
			$this->load->view('employee/register_new_employee');		
		}else{	
					$creat_new_employee = array
					(
					'first_name'            => set_value('fname'),
                    'last_name'             => set_value('lname'),
					'role'		            => 'admin',
					'status'				=> '1'

					);
			          $this->emp_info->create_new_employee($creat_new_employee);
					redirect('producer/products');
			} 
			
		}

	}