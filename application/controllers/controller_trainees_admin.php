<?php
	class Controller_trainees_admin extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->model_trainees_admin();
			$data['trainees'] = null;
			if($query){
				$data['trainees'] = $query;
			}
			$this->load->view("training/view_trainees_admin", $data);
		}
	}
?>