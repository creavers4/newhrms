<?php
class Controller_employee_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $employee_report = $this->model_report->employee_report();
			if($employee_report){
				$data['employee'] = $employee_report;
			}
        $this->load->view("report/view_employee_report",$data);
    }
}
?>