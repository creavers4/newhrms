<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 class Employee_info extends CI_Controller{

     public function __construct ()
	{
		parent::__construct();
		if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
		$this->load->model('emp_info');
		$this->load->model('admin_setting');
		$this->load->model('task_and_evaluation');
		$this->load->model('model_message');
		$this->load->library('unit_test');


		
	
	}
    
    public function index(){
		$data['select_dep_names']=$this->admin_setting->select_dep_names();
		$data['select_jobe_titles']=$this->admin_setting->select_jobe_names();
       $this->load->view('employee/register_new_employee',$data);

    }
	public function view_best_worker_of_my_employee(){
		$data['best_worker']=$this->emp_info->view_best_worker();
		 $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);

			 $data['dep_id'] = $this->admin_setting->find_dep_id_by_session();
			foreach($data['dep_id'] as $dep_ids):
			$dep_id = $dep_ids->dep_id;
			endforeach;
		$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
		$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
		$this->load->view('employee/best_worker',$data);
	}
    public function create(){
		$data['select_dep_names']=$this->admin_setting->select_dep_names();
		$this->form_validation->set_rules('enroll_number','Enroll Number','required');
		$this->form_validation->set_rules('day','Birth Day','required');
		$this->form_validation->set_rules('month','Birth Month','required');
		$this->form_validation->set_rules('year','Birth Year','required');
	    $this->form_validation->set_rules('fname','First Name','required');
        $this->form_validation->set_rules('lname','Last Name','required|alpha_numeric|min_length[2]|max_length[20]');
	    $this->form_validation->set_rules('depName','Department Name','required|alpha_numeric|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('eduLevel','Education Name','required');
	    $this->form_validation->set_rules('phoneNumber','Phone Name','required|alpha_numeric|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('countryName','Country Name','required');
		$this->form_validation->set_rules('jobeTitle','Jobe Name','required|alpha_numeric|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('role','Employee Rolle','required');	
		$this->form_validation->set_rules('username','UserName','required|alpha_numeric|min_length[2]|max_length[20]');	
		$this->form_validation->set_rules('password','Password','required|alpha_numeric|min_length[2]|max_length[20]|md5');	
		$this->form_validation->set_rules('confirmPassword','Confirm Password','required|alpha_numeric|min_length[2]|max_length[20]');	
		$this->form_validation->set_rules('employeeType','Employee type','required|alpha_numeric|min_length[2]|max_length[20]');
		
		
		if ($this->form_validation->run() == FALSE)
		{ 
		//	$this->unit->run(set_value('jobeTitle'),2,"testing jobe name");
			//$this->unit->run(set_value('depName'),2,"testing jobe name");
		//	$this->load->view('test');
			//$this->unit->run('set_value('jobeTitle')','programer','jobe');
			$this->load->view('employee/register_new_employee',$data);		
		}else{	
			$jobeTitle  = set_value('jobeTitle');
		/*	$depName   =set_value('depName');
			$data['dep_id'] = $this->admin_setting->find_dep_id_by_name($depName);
			  foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;
							 */
			$data['dep_id'] = $this->admin_setting->find_dep_id_by_session();

                           foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;
							 $data['select_jobe_titles']=$this->admin_setting->select_jobe_names($dep_id);

			$data['job_id'] = $this->admin_setting->find_jobe_id_by_name($jobeTitle);
             foreach($data['job_id'] as $job_ids):
                            $job_id =   $job_ids->jobe_id; 
                             endforeach;
							 $super_role = set_value('role');
							 if($super_role == "admin"){
								 $super_role = "department head";
							 }elseif($super_role == "admin"){
								 $super_role = "Employee";
							 }else{
								 echo "select admin level";
							 }
							     $day =set_value('day');
			                     $month =	set_value('month');
			                     $year	=	set_value('year');

				      $birt_date = date("Y-m-d", mktime(0,0,0,$month, $day, $year));	
					  $today = date("Y-m-d");
					  $diff = date_diff(date_create($birt_date),date_create($today));
                      $age = $diff->format('%y');
						
					$creat_new_employee = array
					(
					'enroll_number'            => set_value('enroll_number'),
					'first_name'            => set_value('fname'),
                    'last_name'             => set_value('lname'),
					'birth_date'            =>$birt_date,
					'age'                   =>$age,
                    'sex'                   => set_value('sex'),
					'jobe_id'                => $job_id,
					'dep_id'                => $dep_id,
					'edu_level'             => set_value('eduLevel'),
                    'phone_number'          => set_value('phoneNumber'),
					'country_name'          => set_value('age'),
                    'username'              => set_value('username'),
					'employee_type'         => set_value('employeeType'),
					'password'              => set_value('password'),
                    'created_date'          => date('y-m-d'),
					'role'		            => set_value('role'),
					'role_level'		    => $super_role,
					'emp_status'			=> '1'

					);
			          $this->emp_info->create_new_employee($creat_new_employee);
					  $emp_id = $this->db->insert_id();
					  	 if($_FILES['userfile']['name'] != '')
				{	   $config['upload_path']           = './assets/uploads/';
						$config['allowed_types']        = 'jpg|png|.docx';
						$config['max_size']             = 2000;
						$config['max_width']            = 2000;
						$config['max_height']           = 2000;
						$this->load->library('upload', $config);
						
					if ( ! $this->upload->do_upload())
					{	
					
				$this->load->view('admin/document');
						
					}else{
							$upload_image = $this->upload->data();
							$add_documents = array(
								//'document_name'			=> set_value('documentName'),
								'emp_id'			    => $emp_id,
								'reciver_id'		    => $emp_id,
								'file_address'			=> $upload_image['file_name'],
								'document_status'	    => "unread",
								'crated_date'          =>date('y-m-d')
							);
							$this->admin_setting->add_documents($add_documents);
					}
				}
					redirect('employee_info/view_employee');

					
			} 
			
			
		}  public function view_employee(){

	$data['no_bestworker'] = $this->emp_info->count_best_worker();
			
			$data['dep_id'] = $this->admin_setting->find_dep_id_by_session();
			foreach($data['dep_id'] as $dep_ids):
			$dep_id = $dep_ids->dep_id;
			endforeach;
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			if($dep_id == 1){
			$data['select_dep_names']=$this->admin_setting->select_dep_names();
			 $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
			$this->load->view('admin/view_department',$data);	
			}else{
				$data['employees'] =$this->emp_info->view_employees($dep_id);
				$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
				 $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
			 $data['department_name'] = $this->admin_setting->select_dep_names();
			$this->load->view('employee/view_employee',$data);
			 }
	
	
		}
		public function my_profie($emp_id){
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
	  $my_role_level= $data['emp_id']->role_level;
	  $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;
		 $data['profies'] =$this->emp_info->profile($emp_id);
	  foreach($data['profies'] as $dep_ids):
	       $dep_id =   $dep_ids->dep_id;
			$my_role= $dep_ids->role;
		    endforeach;
			$data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;

			if($my_role_level == "employee"){
				if($emp_id==$session_id){
					$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
	  $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$this->load->view('employee/my_profile',$data);
				}else{
					redirect('index');
				}

			}elseif($my_role_level == "department head"){

				if(($emp_id==$session_id) || ($dep_id_role == $my_dep_id)){
					$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
	        $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
	        $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('employee/my_profile',$data);
				}else{
					redirect('index');
				}
			}elseif($my_role_level == "superadmin"){
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
	        $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$this->load->view('employee/my_profile',$data);
				
			}

		/*	if(($emp_id==$session_id) || ($dep_id == $my_dep_id && $my_role !="admin")){
      $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
	  $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
		//$this->load->view('employee/my_profile',$data);
		echo $my_role;
		echo $my_dep_id;
		echo $dep_id;
		echo $emp_id;
		echo $session_id;
		}*/else{
			 redirect('index');
		}
		
		}
		public function employeee_profile($emp_id){
			if( !$this->uri->segment(3) )
            redirect('index');
	    $data['profies'] =$this->emp_info->profile($emp_id);
	    $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
		foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
		$data['notificatio'] = $this->emp_info->count_send_document($dep_id);


$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $login_id =   $data['emp_id']->emp_id;
		endforeach;
		    $data['view_my_doceuments']	=$this->emp_info->view_my_doceument($emp_id,$login_id);

$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
		$this->load->view('employee/my_profile',$data);
		
		}

		public function add_my_document($emp_id){
			if( !$this->uri->segment(3) )
            redirect('index');
		    $data['profies'] =$this->emp_info->profile($emp_id);
			foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$this->load->view('employee/add_my_document',$data);
		}
		public function view_document($emp_id){
			$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
	  $my_role_level= $data['emp_id']->role_level;
	   $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;
			$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $login_id =   $data['emp_id']->emp_id;
		endforeach;
		    $data['view_my_doceuments']	=$this->emp_info->view_my_doceument($emp_id,$login_id);
	        $data['profies'] =$this->emp_info->profile($emp_id);
			foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			$data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;
			 if($my_role_level == "employee"){
				if($emp_id==$session_id){
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$this->load->view('employee/document',$data);
				}
				else{
					redirect('index');
				}
			 } elseif($my_role_level == "department head"){
				if(($emp_id==$session_id) || ($dep_id_role == $my_dep_id)){
			$data['view_my_doceuments']	=$this->emp_info->view_my_doceument($emp_id,$login_id);
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('employee/document',$data);
				}
				else{
					redirect('index');
				}
			}elseif($my_role_level == "superadmin"){
			$data['view_my_doceuments']	=$this->emp_info->view_my_doceument($emp_id,$login_id);
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$this->load->view('employee/document',$data);
                
			}
			else{
					redirect('index');
				}
			
			}
			 
			public function view_send_document($emp_id){
				$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
		endforeach;
			if($emp_id!=$session_id)
         redirect('index');
	        $data['profies'] =$this->emp_info->profile($emp_id);
			foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;

			$data_pdate_document_status = array('document_status' =>"read");
	        $this->emp_info->update_document_status($dep_id,$data_pdate_document_status);
            $data['view_send_doceuments']	=$this->emp_info->view_send_doceument($dep_id);
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('employee/view_send_document',$data);
		}
		public function slip(){
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $emp_id =   $data['emp_id']->emp_id;
		endforeach;
	
	  $data['profies'] =$this->emp_info->profile($emp_id);
		$this->load->view('layout/sidebar',$data);
		}


		 public function view_employee_for_admin($dep_id){
			$data['employees'] =$this->emp_info->view_employees($dep_id);
			$data['department_name'] = $this->admin_setting->select_dep_names();
			 $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			 			 	$data['no_bestworker'] = $this->emp_info->count_best_worker();
			$this->load->view('employee/view_employee',$data);
			 }
		 public function employee_account(){
			 $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
			 foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			 $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			 $data['view_message'] = $this->model_message->view_message($emp_id);
			 $data['view_my_task']=$this->task_and_evaluation->view_task();
			  $data['view_assigned_task']=$this->task_and_evaluation->view_assigned_task();
			  $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			  $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			  $query = $this->task_and_evaluation->view_messageID();
      if($query){
        $data['message'] = $query;
      }
	          $this->load->view('employee/employee_account',$data);
			
			 
		}
		public function disable_emp($emp_id){
			if( !$this->uri->segment(3) )
            redirect('index');
			$active = '0';
			$data_emp = array
		(
			'emp_status'			=> $active
		);
		$this->emp_info->disable_emp($emp_id,$data_emp);
	
		redirect('employee_info/view_employee');

		}
		public function active_employee($emp_id){
			if( !$this->uri->segment(3) )
            redirect('index');
			$active = '1';
			$data_emp = array(
				'emp_status'  => $active
			);
			$this->emp_info->active_emp($emp_id,$data_emp);
			redirect('employee_info/view_employee');
			
		}
		public function change_password()
	{

		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	        $emp_id =   $data['emp_id']->emp_id;
	     	endforeach;
			 
	        $data['profies'] =$this->emp_info->profile($emp_id);
			 foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
$data['notificatio'] = $this->emp_info->count_send_document($dep_id);

             $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
		$this->form_validation->set_rules('oldpassword','Old Password','required|alpha_numeric|md5');
		$this->form_validation->set_rules('password','New Password','required|alpha_numeric|matches[confirmPassword]|min_length[6]|max_length[24]|md5');
		$this->form_validation->set_rules('confirmPassword','Confirm Password','required|alpha_numeric');
		if ($this->form_validation->run() == FALSE)
		{ 
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('employee/change_password',$data);
			
			
		}else{
				$valid_employee	= $this->emp_info->check_password_for_change();
				if($valid_employee	==	FALSE)
					{
						redirect('login/logout');
						$this->session->set_flashdata('error','Username / Password Not Correct !' );
						
						
					}else{
							
							$new_employee_password = array(
														'password'	=> set_value('password')
														);
							
							$this->emp_info->change_eployee_password($new_employee_password);	
							$this->session->set_userdata('username',$valid_employee->username);
							$this->session->set_userdata('role',$valid_employee->role);
							redirect('login');
						 }
				
			 }
	}
	}