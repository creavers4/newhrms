<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Creat_new_admin extends CI_Controller {
	
	public function __construct ()
	{
		parent::__construct();
			if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
		$this->load->model('admin_setting');
		$this->load->model('emp_info');
	}
	public function Edit_employee($emp_id){
			  
	{	
	   $this->form_validation->set_rules('first_name','First Name','required|alpha_numeric|min_length[2]|max_length[20]');
       $this->form_validation->set_rules('last_name','Last Name','required|alpha_numeric|min_length[2]|max_length[20]');
		if ($this->form_validation->run() == FALSE)
			{
				$data['employee'] = $this->emp_info->find($emp_id);
				$this->load->view('admin/edit_employee',$data);
			}else{
					$data_edit_employee = array(
                    'first_name'            => set_value('first_name'),
                    'last_name'             => set_value('last_name')
						
						);
						$this->emp_info->update_employee($emp_id,$data_edit_employee); 
						redirect('employee_info/view_employee');
						
				}
				
			}
			
	

		}
	public function create_new_department()
	  {
    
	$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();

		   foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
	   $this->form_validation->set_rules('depName','Department Name','required|alpha_numeric|min_length[2]|max_length[20]');
		if($this->form_validation->run()	==	FALSE)
		{
	$this->load->view('admin/create_new_department',$data); 
		}else{
				$data_new_department = array
				 (
				 	'dep_name'                      => set_value('depName'),
					'created_date'		        	=> date('y-m-d-')

				 );
				 $this->admin_setting->create_new_department($data_new_department);
				 redirect('creat_new_admin/select_all_dep_names');
				 
			
			}
			
		}


		public function select_dep_names(){
			$data['select_dep_names']=$this->admin_setting->select_dep_names();
			$this->load->view('admin/create_new_jobe',$data);
			
		}
			public function select_all_dep_names(){
			$data['select_dep_names']=$this->admin_setting->select_dep_names();
			$this->load->view('admin/view_department',$data);
			
		}
		public function select_jobe_names(){
			$data['select_jobe_names']=$this->admin_setting->select_jobe_names();
			$this->load->view('admin/create_new_jobe',$data);
		}
        public function select_all_jobe_info(){
			$data['select_jobe_names']=$this->admin_setting->select_jobe_info();
			$this->load->view('admin/view_job',$data);
		}

		public function create_new_jobe()
	  {
		  $data['select_dep_names']=$this->admin_setting->select_dep_names();


		  $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();

		   foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
	   $this->form_validation->set_rules('depName','Department Name','required|alpha_numeric|min_length[2]|max_length[20]');
	   $this->form_validation->set_rules('jobeTitle','Jobe Title','required|alpha_numeric|min_length[2]|max_length[20]');
	   $this->form_validation->set_rules('jobeDiscription','Jobe Description','required|min_length[2]|max_length[20]');
	   $this->form_validation->set_rules('basic_sallary','Basic Sallary','required|alpha_numeric|min_length[2]|max_length[20]');
		if($this->form_validation->run()	==	FALSE)
		{
		$this->load->view('admin/create_new_jobe',$data); 
		}else{
			$depName  = set_value('depName');
			$data['dep_id'] = $this->admin_setting->find_dep_id_by_name($depName);
             foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;
				$data_new_jobe = array
				 (
				    'dep_id'                         => $dep_id,
				 	'jobe_title'                     => set_value('jobeTitle'),
					'jobe_description'                => set_value('jobeDiscription'),
					'creted_date'		        	=> date('y-m-d-')
				 );
			$this->admin_setting->create_new_jobe($data_new_jobe);	
			$job_id =$this->db->insert_id();

			$salary = set_value('basic_sallary');
			if($salary >=0 && $salary <600){
				$tax = 0;
				}elseif($salary >=601 && $salary <1650){
					$tax =$salary * 0.1;

				}elseif($salary >=1651 && $salary <3200){
						$tax =$salary * 0.15;

				}
				elseif($salary >=3200 && $salary <5250){
						$tax =$salary * 0.20;

				}
				elseif($salary >=5251 && $salary <7800){
						$tax =$salary * 0.25;

				}elseif($salary >=7801 && $salary <10900){
						$tax =$salary * 0.30;

				}elseif($salary >=10900){
						$tax =$salary * 0.35;

				}
         $data_salary = array(
					'basic_sallary'                       => set_value('basic_sallary'),
					'jobe_id'                             => $job_id,
					'tax'                                  => $tax

				 );
				 $this->emp_info->add_employee_sallary($data_salary);
			redirect('creat_new_admin/select_all_jobe_info');
			}
		
		
			 
		}
		public function report(){
		$data['view_administretors'] = $this->admin_setting->select_administretor();
		$this->load->view('admin/view_administretor',$data);

		}
			public function add_documents($emp_id)   
	{
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
	   $my_role_level= $data['emp_id']->role_level;
	   $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;
		
		     $data['select_dep_names']=$this->admin_setting->select_dep_names();
			 $data['profies'] =$this->emp_info->profile($emp_id);
			 foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			$data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;
			 $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			 $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
		      $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
		$this->form_validation->set_rules('documentName','Document Name','required');
		$this->form_validation->set_rules('deocument_type','Document Type','required');
	    $this->form_validation->set_rules('depName','Department Name','required');
			 if($my_role_level == "department head"){
				if($emp_id==$session_id){
		if ($this->form_validation->run() == FALSE)
			{
				
				$this->load->view('admin/document',$data);
			} else {
				if($_FILES['userfile']['name'] != '')
				{	   $config['upload_path']           = './assets/uploads/';
						$config['allowed_types']        = 'jpg|jpeg|png|doc|txt|pdf|sql';
						$config['max_size']             = 2000;
						$config['max_width']            = 2000;
						$config['max_height']           = 2000;
						$this->load->library('upload', $config);
						
					if ( ! $this->upload->do_upload())
					{	
					
				$this->load->view('admin/document');
						
					}else{
						$depName = set_value('depName');
						$data['dep_id'] = $this->admin_setting->find_dep_id_by_name($depName);
						 foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;
		 $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	    $emp_id =   $data['emp_id']->emp_id;
		endforeach;
							$upload_image = $this->upload->data();
							$add_documents = array(
								'document_name'			=> set_value('documentName'),
								'document_type'			=> set_value('deocument_type'),
								'file_address'			=> $upload_image['file_name'],
								'dep_id'                 =>$dep_id,
								'emp_id'                 =>$emp_id,
								'sender_id'              =>$emp_id,
								'document_status'         =>"unread",
								'crated_date'             =>date('y-m-d')
							);
							$this->admin_setting->add_documents($add_documents);
							redirect('employee_info/view_send_document/'.$emp_id);
					}
				}
				
			}
			
}
}else{
					redirect('index');
				}
		
		}
}
