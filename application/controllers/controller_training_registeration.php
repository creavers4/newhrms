				<?php
	class Controller_training_registeration extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$this->load->view('training/view_training_registeration');

		}
		public function register_trainings(){
			$this->form_validation->set_rules('training', 'Training', 'required');
			$this->form_validation->set_rules('fee', 'Fee', 'required');
			if ($this->form_validation->run()==FALSE){
				$this->load->view('training/view_training_registeration');
			}else{
				$data = array('training_id'  				=> set_value('trainingID'),
						      'training'         			=> set_value('training'),
							  'vendor' 	   					=> set_value('vendor'),
							  'specify_schedule'			=> set_value('specify_schedule'),
							  'fee'       					=> set_value('fee')
					);
				$this->model_training->model_training_insert($data);
				redirect('controller_training_registeration');
			}
		}
		
	}
?>