<?php
class Controller_staff_career_development extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->trainings_drop_down();
			if($query){
				$data['trainings'] = $query;
			}

			$query1 = $this->model_training->trainers_drop_down();
			if($query1){
				$data['trainer'] = $query1;
			}
			$this->load->view('training/staff_career_development',$data);

		}public function staff_career_development(){
			$this->form_validation->set_rules('staff_id', 'Staff ID', 'required');
			$this->form_validation->set_rules('fName', 'First Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('class', 'Class', 'required');
			$this->form_validation->set_rules('lName', 'Last name', 'required');
			$this->form_validation->set_rules('schedule_fee', "Class schedule & fee", 'required');
			$this->form_validation->set_rules('department', "Department", 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('training/controller_staff_career_development');
			}else{
				$data = array('staff_id'         		=> set_value('staff_id'),
							  'fname'           		=> set_value('fName'),
							  'lname'         			=> set_value('lName'),
							  'phone'           		=> set_value('phone'),
							  'email' 					=> set_value('email'),
							  'department'           	=> set_value('department'),
							  'class'           		=> set_value('class'),
							  'schedule_fee'           	=> set_value('schedule_fee')
							  
					);
				$this->model_training->staff_career_development($data);
				redirect('controller_staff_career_development');
			}
		}
		
	}
?>
		