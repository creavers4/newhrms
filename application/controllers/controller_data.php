<?php
class Controller_data extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_performance');
		}
    public function index(){
        $employee_attendance = $this->model_performance->get_social_interaction();
			if($employee_attendance){
				$data['attendance'] = $employee_attendance;
			}
        $this->load->view("view_data", $data);
    }
}
?>