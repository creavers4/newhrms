<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payrolls extends CI_Controller {
	function __construct()
 	{
 		parent::__construct();
		 if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
	$this->load->model('payroll');
	$this->load->model('emp_info');
	$this->load->model('admin_setting');
	}
    public function index(){
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();

		   foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
     $data['salariys'] = $this->payroll->select_sallary();
     $this->load->view('payroll/salary',$data);
    }


	public function edit_sallary($emp_id){
		if( !$this->uri->segment(3) )
            redirect('index');		  
	 //  $this->form_validation->set_rules('basic_sallary','Basic Salary','required|alpha_numeric|min_length[2]|max_length[20]');
	    $this->form_validation->set_rules('over_time','Over Time','required|min_length[2]|max_length[20]');
	//	 $this->form_validation->set_rules('other','Other','required');
		if ($this->form_validation->run() == FALSE)
			{
				$data['edit_salary'] =$this->payroll->find_emp_salary($emp_id);
			$this->load->view('payroll/edit_sallary',$data);
			
			}else{
				$data['edit_salary'] =$this->payroll->select_emp_id($emp_id);
				foreach($data['edit_salary'] as $emp_info):
				$basic_salary = $emp_info->basic_sallary;
				$over_time =$emp_info->over_times;

				$over_time_percent =set_value('over_time');
				if($over_time_percent == 0.0125){
					$basic_salary = $basic_salary * 0.0125;
					$over_time = $over_time + $basic_salary;
				}elseif($over_time_percent == 0.02){
					$basic_salary = $basic_salary * 0.02;
					$over_time = $over_time + $basic_salary;

				}elseif($over_time_percent == 0.025){
					$basic_salary = $basic_salary * 0.025;
					$over_time = $over_time + $basic_salary;

				}
					$data_edit_emp_sallary = array(
                   //   'basic_sallary'             => set_value('basic_sallary'),
					  'over_time'                => $over_time,
					  'other'                   => set_value('other')
						
						);
						$salary_id = $emp_info->salary_id;
						$this->payroll->update_emp_sallary($salary_id,$data_edit_emp_sallary); 
						$data_edit_over_time = array(
					  'over_times'                => $over_time
						);
						$this->payroll->update_over_time($emp_id,$data_edit_over_time);
						$data_insert_emp_over_time = array(
					  'over_time_salary'                => $over_time,
					  'date'                => date('y-m-d')

							);
							$this->payroll->data_insert_emp_over_time($data_insert_emp_over_time);
					
						endforeach;
							redirect('payrolls');
				}
				
			}
			public function employee_slip($emp_id){
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
	   $my_role_level= $data['emp_id']->role_level;
	   $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;
			$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
		    $data['profies'] =$this->emp_info->profile($emp_id);
			foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			$data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;

                if($my_role_level == "employee"){
				if($emp_id==$session_id){
			$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
            $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('payroll/employee_slip',$data);
				}else{
					redirect('index');
				}

			}elseif($my_role_level == "department head"){
				if(($emp_id==$session_id) || ($dep_id_role == $my_dep_id)){
			$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			$this->load->view('payroll/employee_slip',$data);

			}else{
					$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			$this->load->view('payroll/employee_slip',$data);
				}
			}elseif($my_role_level == "superadmin"){
			$data['employee_slips']	=$this->payroll->select_emp_id($emp_id);
			//$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$this->load->view('payroll/employee_slip',$data);
                
			}
			else{
					redirect('index');
				}
			
			}
			public function find_emp_id_by_user_name(){
				$data['find_emp_id_by_user_names'] =$this->emp_info->find_emp_id_by_user_name();
				$this->load->view('layout/navigation',$data);
			}
}
