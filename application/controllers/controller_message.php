<?php
    class Controller_message extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("model_message");
        }
        public function index(){
			$query = $this->model_message->employee_id();
      		if($query){
        	$data['employee'] = $query;
      	}
            $this->load->view("view_message", $data);
        }
		public function update_approval(){
			$message_i = set_value('message_i');
			$update = array('approval' 	=> set_value('message'));
			$this->model_message->update_approval($update, $message_i);
			redirect('employee_info/employee_account');
		}
        public function send_message(){
            $this->form_validation->set_rules('sender_id', 'Sender ID', 'required');
			$this->form_validation->set_rules('staff_id', 'Staff ID', 'required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			$this->form_validation->set_rules('message', 'Message', 'required');
			if ($this->form_validation->run()==FALSE){
				$this->load->view('view_message');
			}else{
				$data = array('date'         		=> date("Y/m/d"),
								'message'         		=> set_value('message'),
							  'reciver_id'           	=> set_value('staff_id'),
							  'sender_id'         		=> set_value('sender_id'),
							  'subject' 	   			=> set_value('subject'),
							  'message_status'       	=> 'read'
							  
					);
				$this->model_message->send_message($data);
				redirect('controller_message');
        }
    }
    }