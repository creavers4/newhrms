<?php
class Controller_attendance_enroll_no_5 extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $enroll_no_5 = $this->model_report->attendance_enroll_no_5();
			if($enroll_no_5){
				$data['enroll_no'] = $enroll_no_5;
			}
        $this->load->view("report/view_enroll_no_5",$data);
    }
}
?>