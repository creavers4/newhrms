<?php
class Controller_trainers_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $trainers_report = $this->model_report->trainers_report();
			if($trainers_report){
				$data['trainers'] = $trainers_report;
			}
        $this->load->view("report/view_trainers_report",$data);
    }
}
?>