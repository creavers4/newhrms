		<?php
	class Controller_trainee_registeration extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->trainings_drop_down();
			if($query){
				$data['trainings'] = $query;
			}

			$query1 = $this->model_training->trainers_drop_down();
			if($query1){
				$data['trainer'] = $query1;
			}
			$this->load->view('training/view_trainee_registeration',$data);

		}
		public function update_status(){
			$update = array('status' 	=> 'contract_completed');
			$this->model_training->update_status($update);
			redirect('controller_trainees_view');
		}
		public function register_trainees(){
			$this->form_validation->set_rules('traineeID', 'Trainee ID', 'required');
			$this->form_validation->set_rules('fName', 'First Name', 'required');
			$this->form_validation->set_rules('sex', 'Sex', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('class', 'Class', 'required');
			$this->form_validation->set_rules('lName', 'Last name', 'required');
			$this->form_validation->set_rules('age', "Age", 'required');
			$this->form_validation->set_rules('schedule_fee', "Class schedule & fee", 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('training/view_trainee_registeration');
			}else{
				$data = array('traineeId'         		=> set_value('traineeID'),
							  'fname'           		=> set_value('fName'),
							  'lname'         			=> set_value('lName'),
							  'sex' 	   				=> set_value('sex'),
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'email' 					=> set_value('email'),
							  'city'           			=> set_value('city'),
							  'company'           		=> set_value('company'),
							  'training_id'           	=> set_value('training_id'),
							  'class'           		=> set_value('class'),
							  'schedule_fee'           	=> set_value('schedule_fee'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')
							  
					);
				$this->model_training->model_trainee_insert($data);
				redirect('controller_trainee_registeration');
				
			}
		}
		
	}
?>
		