<?php
class Controller_evaluation_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $evaluation_report = $this->model_report->evaluation_report();
			if($evaluation_report){
				$data['evaluation'] = $evaluation_report;
			}
        $this->load->view("report/view_evaluation_report",$data);
    }
}
?>