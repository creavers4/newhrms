<?php
class Controller_trainings_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $trainings_report = $this->model_report->trainings_report();
			if($trainings_report){
				$data['trainings'] = $trainings_report;
			}
        $this->load->view("report/view_trainings_report",$data);
    }
}
?>