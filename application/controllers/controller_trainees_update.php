<?php
	class Controller_trainees_update extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->trainees_view_for_update();
			if($query){
				$data['trainees_update'] = $query;
			}
			$query2 = $this->model_training->trainings_drop_down();
			if($query2){
				$data['trainings'] = $query2;
			}

			$query1 = $this->model_training->trainers_drop_down();
			if($query1){
				$data['trainer'] = $query1;
			}
			$this->load->view('training/view_trainees_update', $data);

		}
		public function update_trainees(){
			$select = set_value('select_trainees');
			$update = array(  'phone'           		=> set_value('phone'),
							  'email' 					=> set_value('email'),
							  'city'           			=> set_value('city'),
							  'company'           		=> set_value('company'),
							  'class'           		=> set_value('class'),
							  'schedule_fee'           	=> set_value('schedule_fee'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')
							);

			$this->model_training->update_trainees($update,$select);
			redirect('controller_trainees_update');
		}
		}
		

?>