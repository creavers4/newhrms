<?php
    class Controller_evaluation_approval extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("task_and_evaluation");
        
        }
		public function update_approval(){
			$evaluation_id = set_value('evaluation_id');
			$update = array('approval' 	=> set_value('approval'));
			$this->task_and_evaluation->update_approval($update, $evaluation_id);
			redirect('employee_info/employee_account');
		}
        public function send_message(){
            $this->form_validation->set_rules('sender_id', 'Sender ID', 'required');
			$this->form_validation->set_rules('staff_id', 'Staff ID', 'required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			$this->form_validation->set_rules('message', 'Message', 'required');
			if ($this->form_validation->run()==FALSE){
				$this->load->view('view_message');
			}else{
				$data = array('message'         		=> set_value('message'),
							  'reciver_id'           	=> set_value('staff_id'),
							  'sender_id'         		=> set_value('sender_id'),
							  'subject' 	   			=> set_value('subject'),
							  'message_status'       	=> 'read'
							  
					);
				$this->model_message->send_message($data);
				redirect('controller_message');
        }
    }
    }