<?php
class Controller_attendance_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $attendance_report = $this->model_report->attendance_report();
			if($attendance_report){
				$data['attendance'] = $attendance_report;
			}
        $this->load->view("report/view_attendance_report",$data);
    }
}
?>