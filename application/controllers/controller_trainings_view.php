<?php
	class Controller_trainings_view extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->model_trainings_view();
			$data['trainings'] = null;
			if($query){
				$data['trainings'] = $query;
			}
			$this->load->view("training/view_trainings", $data);
		}
	}
?>