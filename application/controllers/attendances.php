<?php
class Attendances extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
		$this->load->model('attendance');
        $this->load->model('emp_info');
		$this->load->model('admin_setting');
        	date_default_timezone_set('Africa/Addis_Ababa'); 
	
	} 
    public function index(){
		$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();

		   foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);

        $data['view_attendances'] = $this->attendance->view_attendance();
        $this->load->view('attendance/view_attendance',$data);
     }

     public function my_attendance($emp_id){
	$data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
	   $my_role_level= $data['emp_id']->role_level;
	   $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;

		 $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
	    $data['profies'] =$this->emp_info->profile($emp_id);
		foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
			$data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;
			if($my_role_level == "employee"){
				if($emp_id==$session_id){
        $data['my_attendances'] = $this->attendance->view_my_attendance($emp_id);
		$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
		$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
        $this->load->view('attendance/my_attendance',$data);
     }else{
					redirect('index');
				}
			}elseif($my_role_level == "department head"){
				if(($emp_id==$session_id) || ($dep_id_role == $my_dep_id)){
			$data['my_attendances'] = $this->attendance->view_my_attendance($emp_id);
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			 $this->load->view('attendance/my_attendance',$data);

			}else{
					redirect('index');
				}
			}elseif($my_role_level == "superadmin"){
			 $data['my_attendances'] = $this->attendance->view_my_attendance($emp_id);
			$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
			$data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
			 $this->load->view('attendance/my_attendance',$data);
                
			}
			else{
					redirect('index');
				}
	 }
     public function time_caculation(){
$c_h = idate("h");
$c_i = idate("i");
$c_s = idate("s");
$current_time = idate("h"). ':' .idate("i"). ':' .idate("s");
echo "current time: ".$current_time;
echo "</br>";
echo "<br>";
$c_h =$c_h *3600;
$c_i =$c_i *60; 
$current_total_secod =$c_h + $c_i+$c_s;
if($current_total_secod > 9000 && $current_total_secod < 23400 ){
	$constant_second = 9000;
	echo "morning cheak in duty off ";

//trial
echo "</br>";
echo "current total second ". $current_total_secod;
echo "</br>";
echo "current default second ".  $resualt = $constant_second;


echo "</br>";
$second_diff =  $current_total_secod - $constant_second;
echo "second difference  ".$resualt = $current_total_secod - $constant_second;

echo "</br>";
if($resualt >= 3600){
	echo "</br>";
	$hour =  intval($resualt/3600);
	echo "</br>";
	$m =  $resualt % 3600;
	echo "</br>";
	echo "</br>";
	$minute = intval($m/60);
	echo "</br>";
	if($m >= 60){

		$second  = $m % 60;
	echo "</br>";
 $time_diff =$hour. ":". intval($minute) . ":" .$second;
	echo "Time difference".$time_diff;
    $data_resualt = array(
        'dutty_off'  => $time_diff,
         'check_type' =>"morning chack _in",
         'check_in_date' => date('y-m-d'),
         'check_in_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);

	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'morning_check_in' =>$current_time,
		 'm_in_duty_off'   =>$second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}
}

}elseif ($resualt >= 60 && $resualt <3600) {
	echo "</br>";
	$hour =intval($resualt / 3600); 
	$minute= $resualt / 60;
	$second = $resualt % 60;
	echo "</br>";
	echo "</br>";
	echo "<br>";
	echo "<br>";
	$time_diff = $hour. ":". intval($minute) . ":" .$second; 
	echo "time difference: ".$time_diff;
     $data_resualt = array(
        'dutty_off'  => $time_diff,
         'check_type' =>"morning chack _in",
         'check_in_date' => date('y-m-d'),
         'check_in_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	
       // 'dutty_off'  => $time_diff,
         //'check_type' =>"morning chack _in",
		 $data['duty_off'] = $this->attendance->find_duty_off_by_date();
		 $duty_offs = $data['duty_off']->duty_off;
		$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'morning_check_in' =>$current_time,
		 'm_in_duty_off'   =>$second_diff,
		 'duty_off'  =>  $duty_offs +  $second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}
}else{
	echo $resualt;
    $data_resualt = array(
        'dutty_off'  => $resualt,
         'check_type' =>"morning chack _in",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time,
		 'duty_off'  =>  $duty_offs +  $second_diff

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);


}

//okay

}elseif($current_total_secod > 23400 && $current_total_secod < 27000 ){
	$constant_second = 23400;
	echo "morning cheak out duty on over time";

    echo "</br>";
echo "current total second ". $current_total_secod;
echo "</br>";
echo "current default second ".  $resualt = $constant_second;


echo "</br>";
$second_diff =$current_total_secod - $constant_second;
echo "second difference  ".$resualt = $current_total_secod - $constant_second;

echo "</br>";
if($resualt >= 3600){
	echo "</br>";
	$hour =  intval($resualt/3600);
	echo "</br>";
	$m =  $resualt % 3600;
	echo "</br>";
	$minute = intval($m/60);
	echo "</br>";
	if($m >= 60){

	$second  = $m % 60;
	echo "</br>";
   $time_diff = $hour. ":". intval($minute) . ":" .$second;
	echo "Time difference".$time_diff;
    $data_resualt = array(
        'dutty_off'  => $time_diff,
         'check_type' =>"morning chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
		$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	$data['m_check_out'] =  $this->attendance->find_m_check_out_by_date();
	foreach($data['m_check_out']  as $m_check_out):
    $m_check_out =	$m_check_out->morning_check_out;
	$duty_on = 28800 - $m_in_dutty_off - $a_in_duty_off + $m_out_dutty_on +$a_out_duty_on;
	endforeach;
	echo  "samisams";
	if($m_check_out == '00:00:00'){
		echo  "samisams";
		if($m_check_in){
          $data_resualt2 = array(
         'morning_check_out' =>$current_time,
		 'm_out_duty_on'   =>$second_diff,
		 'duty_on' =>  $duty_on 
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	
	}



	}

}elseif ($resualt >= 60 && $resualt <3600) {
	echo "</br>";
	$hour =intval($resualt / 3600); 
	$minute= $resualt / 60;
	$second = $resualt % 60;
	echo "</br>";
	echo "</br>";
	echo "<br>";
	echo "<br>";
	$time_diff = $hour. ":". intval($minute) . ":" .$second; 
	echo "time difference: ".$time_diff;
    $data_resualt = array(
        'dutty_off'  => $time_diff,
         'check_type' =>"morning chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$second_diff

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	$data['m_check_out'] =  $this->attendance->find_m_check_out_by_date();
	foreach($data['m_check_out']  as $m_check_out):
    $m_check_out =	$m_check_out->morning_check_out;
	//$duty_on = $m_check_out->morning_check_out;
	 //$m_check_out =	  $m_check_out->morning_check_out;
	 // $m_check_m_in_duty_off =	$m_check_out->duty_on;
	//   $m_check_out =	$m_check_out->morning_check_out;
	//    $m_check_out =	$m_check_out->morning_check_out;
	endforeach;
	//echo $m_check_m_in_duty_off;
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'morning_check_in' =>$current_time,
		 'm_out_duty_on'   =>$second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}else{
	if($m_check_out == '00:00:00'){
		if($m_check_in){
          $data_resualt2 = array(
         'morning_check_out' =>$current_time,
		 'm_out_duty_on'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	}
	
	}


}else{
	echo $resualt;
    $data_resualt = array(
        'dutty_off'  => $resualt,
         'check_type' =>"morning chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if($m_check_in){
          $data_resualt2 = array(
         'morning_check_out' =>$current_time,
		 'm_out_duty_on'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}


}

}elseif($current_total_secod > 27000 && $current_total_secod < 41400 ){
	$constant_second = 27000;
	echo "afternoons check in duty off";

    echo "</br>";
echo "current total second ". $current_total_secod;
echo "</br>";
echo "current default second ".  $resualt = $constant_second;


echo "</br>";
$second_diff = $current_total_secod - $constant_second;
echo "second difference  ".$resualt = $current_total_secod - $constant_second;

echo "</br>";
if($resualt >= 3600){
	
	//  "mmodule".$resualt % 3600;
	echo "</br>";
	//echo "hour".$resualt / 3600; 
	//echo "</br>";
	$hour =  intval($resualt/3600);
	echo "</br>";
	$m =  $resualt % 3600;
	echo "</br>";

	//echo "minute".$m/60;
	echo "</br>";
	$minute = intval($m/60);
	echo "</br>";
	if($m >= 60){

		$second  = $m % 60;
	echo "</br>";
  $time_diff =  $hour. ":". intval($minute) . ":" .$second;
	echo "Time difference".$time_diff;

    $data_resualt = array(
         'dutty_off'  => $time_diff,
         'check_type' =>"After noon chack _in",
         'check_in_date' => date('y-m-d'),
         'check_in_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	//$A_check_in =  $this->attendance->find_A_check_in_by_date();

	//
	$data['A_check_in'] =  $this->attendance->find_A_check_in_by_date();
	foreach($data['A_check_in']  as $A_check_in):
	echo $A_check_in->afternoon_check_in; 
    $A_check_in =	$A_check_in->afternoon_check_in;
	endforeach;
	//
	 $data['duty_off'] = $this->attendance->find_duty_off_by_date();
		 $duty_offs = $data['duty_off']->duty_off;
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'afternoon_check_in' =>$current_time,
		 'a_in_duty_off'   =>$second_diff,
		 'duty_off'  =>$second_diff + $duty_offs
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}else{
	
	if($A_check_in == '00:00:00'){
		if($m_check_in){
          $data_resualt2 = array(
         'afternoon_check_in' =>$current_time,
		 'a_in_duty_off'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	}
		}



	}

}elseif ($resualt >= 60 && $resualt <3600) {
	echo "</br>";
	$hour =intval($resualt / 3600); 
	$minute= $resualt / 60;
	$second = $resualt % 60;
	echo "</br>";
	echo "</br>";
	echo "<br>";
	echo "<br>";
	$time_diff = $hour. ":". intval($minute) . ":" .$second; 
	echo "time difference: ".$time_diff;

      $data_resualt = array(
         'dutty_off'  => $time_diff,
         'check_type' =>"After noon chack _in",
         'check_in_date' => date('y-m-d'),
         'check_in_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	//$A_check_in =  $this->attendance->find_A_check_in_by_date();

	//
	$data['A_check_in'] =  $this->attendance->find_A_check_in_by_date();
	foreach($data['A_check_in']  as $A_check_in):
	echo $A_check_in->afternoon_check_in; 
    $A_check_in =	$A_check_in->afternoon_check_in;
	endforeach;
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'afternoon_check_in' =>$current_time,
		 'm_in_duty_off'   =>$second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}else{
	if($A_check_in == '00:00:00'){
		if($m_check_in){
          $data_resualt2 = array(
         'afternoon_check_in' =>$current_time,
		 'a_in_duty_off'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	}
		}

}else{
      $data_resualt = array(
        'dutty_off'  => $resualt,
         'check_type' =>"After noon chack _in",
         'check_in_date' => date('y-m-d'),
         'check_in_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	echo $resualt;


}

}elseif($current_total_secod > 41400){
	$constant_second = 41400;
	echo "afnoonter cheak out over tim";
    echo "</br>";
echo "current total second ". $current_total_secod;
echo "</br>";
echo "current default second ".  $resualt = $constant_second;


echo "</br>";
$second_diff =$current_total_secod - $constant_second;
echo "second difference  ".$resualt = $current_total_secod - $constant_second;

echo "</br>";
if($resualt >= 3600){
	
	//  "mmodule".$resualt % 3600;
	echo "</br>";
	//echo "hour".$resualt / 3600; 
	//echo "</br>";
	$hour =  intval($resualt/3600);
	echo "</br>";
	$m =  $resualt % 3600;
	echo "</br>";

	//echo "minute".$m/60;
	echo "</br>";
	$minute = intval($m/60);
	echo "</br>";
	if($m >= 60){

		$second  = $m % 60;
	echo "</br>";
      $time_diff = $hour. ":". intval($minute) . ":" .$second;
	echo "Time difference".$time_diff; 

    $data_resualt = array(
        'over_time'  => $time_diff,
         'check_type' =>"After noon chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);
	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	$data['A_check_out'] =  $this->attendance->find_A_check_out_by_date();
	//$A_check_out1 =implode(" ", $A_check_out);
	foreach($data['A_check_out']  as $A_check_out):
	echo $A_check_out->afternoon_check_out; 
$A_check_out =	$A_check_out->afternoon_check_out;
	endforeach;

	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'afternoon_check_out' =>$current_time,
		 'm_in_duty_off'   =>$second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}else{
	
	if($A_check_out  == '00:00:00'){
		//echo $A_check_out;
		if($m_check_in){
          $data_resualt2 = array(
         'afternoon_check_out' =>$current_time,
		  'a_out_duty_on'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	}


	}
	}

}elseif ($resualt >= 60 && $resualt <3600) {
	echo "</br>";
	$hour =intval($resualt / 3600); 
	$minute= $resualt / 60;
	$second = $resualt % 60;
	echo "</br>";
	echo "</br>";
	echo "<br>";
	echo "<br>";
	$time_diff = $hour. ":". intval($minute) . ":" .$second; 
	echo "time difference: ".$time_diff;

$data_resualt = array(
        'over_time'  => $time_diff,
         'check_type' =>"After noon chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);

	$m_check_in =  $this->attendance->find_mcheck_in_by_date();
	$data['A_check_out'] =  $this->attendance->find_A_check_out_by_date();
	//$A_check_out1 =implode(" ", $A_check_out);
	foreach($data['A_check_out']  as $A_check_out):
	echo $A_check_out->afternoon_check_out; 
$A_check_out =	$A_check_out->afternoon_check_out;
	endforeach;
		$m_check_in =  $this->attendance->find_mcheck_in_by_date();
		if(!$m_check_in){
          $data_resualt2 = array(
         'record_date' => date('y-m-d'),
         'afternoon_check_out' =>$current_time,
		 'm_in_duty_off'   =>$second_diff
    );
	$this->attendance->my_daily_attendance_resualt($data_resualt2);
		}else{
	if($A_check_out  == '00:00:00'){
		//echo $A_check_out;
		if($m_check_in){
          $data_resualt2 = array(
         'afternoon_check_out' =>$current_time,
		 'a_out_duty_on'   =>$second_diff
    );
	$this->attendance->update_daily_attendance_resualt($data_resualt2);
		}
	}
}

}else{
	echo $resualt;

    $data_resualt = array(
        'over_time'  => $time_diff,
         'check_type' =>"After noon chack _out",
         'check_in_date' => date('y-m-d'),
         'check_out_time' =>$current_time

    );
    $this->attendance->insert_my_attendance_resualt($data_resualt);


}

}
 
 }
 public function view_new_attendance(){
     $data['view_new_attendances'] = $this->attendance->view_new_attendance();
     $this->load->view('attendance/new_attendance',$data);
 }

}