<?php
	class Controller_trainers_update extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->trainers_view_for_update();
			if($query){
				$data['trainers_update'] = $query;
			}
			$query1 = $this->model_training->trainings_drop_down();
			if($query1){
				$data['trainings'] = $query1;
			$this->load->view('training/view_trainers_update', $data);

		}
	}
		public function update_trainers(){
			$select = set_value('select_trainers');
			$update = array(
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'city' 					=> set_value('city'),
							  'email'           		=> set_value('email'),
							  'training'           		=> set_value('training'),
							  'experience'           	=> set_value('experience'),
							  'specialized_field'       => set_value('specialized_field'),
							  'specific_stay'           => set_value('specific_stay'),
							  'salary'                	=> set_value('salary'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')
							  );

			$this->model_training->update_trainers($update,$select);
			redirect('controller_trainers_update');
		}
		}
		

?>