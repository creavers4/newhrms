<?php
    class Controller_message_view extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("model_message");
        }
        public function index(){
           $query = $this->model_message->view_staff_message();
			$data['message'] = null;
			if($query){
				$data['message'] = $query;
			}
			
			$this->load->view("view_message_view", $data);
        }
    }