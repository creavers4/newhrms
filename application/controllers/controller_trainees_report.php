<?php
class Controller_trainees_report extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('model_report');
		}
    public function index(){
        $trainees_report = $this->model_report->trainees_report();
			if($trainees_report){
				$data['trainees'] = $trainees_report;
			}
        $this->load->view("report/view_trainees_report",$data);
    }
}
?>