<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
 	{
 		parent::__construct();
	$this->load->model('admin_setting');
	$this->load->model('emp_info');
	date_default_timezone_set('Africa/Addis_Ababa'); 
	}
		
	public function index()
	{
		
		$this->form_validation->set_rules('username','Username','required|alpha_numeric');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric|md5');
		if($this->form_validation->run()==	FALSE)
		{
        
	        $this->load->view('login'); 

			}else{
				
				$valid_user	= $this->emp_info->check_emp_employee();
			

			
	    if($valid_user	==	FALSE)
			{
				$check_employee_is_active = $this->emp_info->check_employee_is_active();
				if ($check_employee_is_active == FALSE)
				{
						$this->session->set_flashdata('error','Username / Password Not Correct !' );
				}else{
						$this->session->set_flashdata('error','Sorry this account is not active !' );
					 }
				
				redirect('login');
			}else{
				$this->session->set_userdata('username',$valid_user->username);
				$this->session->set_userdata('role',$valid_user->role);
				$this->session->set_userdata('role_level',$valid_user->role_level);
				
				$date = date('y-m-d h:i:s',now());
				$my_time =date('h:i:s',now());
				if($my_time >'02:30:00' && $my_time < '06:30:00' ){
					$check_in = array(
					'check_in_date' =>$date,
					'check_in_time' =>$my_time,
					'check_type'   =>"Mornig_cheak_in_"
				);
				$this->emp_info->check_in($check_in);

				}elseif($my_time >'06:30:00' && $my_time < '07:30:00' ){
					$check_in = array(
					'check_in_date' =>$date,
					'check_in_time' =>$my_time,
					'check_type'   =>"Mornig_cheak_out"
				);
				$this->emp_info->check_in($check_in);

				}elseif($my_time > '07:30:00' && $my_time < '11:30:00' ){
					$check_in = array(
					'check_in_date' =>$date,
					'check_in_time' =>$my_time,
					'check_type'   =>"Afternoon_cheak_in"
				);
				$this->emp_info->check_in($check_in);
				}elseif($my_time >'11:30:00'){
					$check_in = array(
					'check_in_date' =>$date,
					'check_in_time' =>$my_time,
					'check_type'   =>"Mornig_cheak_out"
				);
				$this->emp_info->check_in($check_in);
				}else{
					$check_in = array(
					'check_in_date' =>$date,
					'check_in_time' =>$my_time,
					'check_type'   =>"Mornig_cheak_out"
				);
				$this->emp_info->check_in($check_in);

				}
					
				

				}
				switch($valid_user->role){
				case 'admin';
				redirect('employee_info/view_employee');	 
                break;
				case 'employee';
				redirect('employee_info/employee_account');	
                break;
				default:break;
				
					
				}
					

			
		}
		
		}
	public function logout()
	{	$date = date('y-m-d');
        $my_time =date('h:i:s');
	$hello =	strtotime(strtotime( $my_time) +  strtotime( $my_time ));

		if($my_time > '06:30:01' && $my_time < '07:30:00'){
					$check_out = array(
					'check_in_date' =>$date,
					'check_out_time' =>$my_time,
					'check_type'   =>"Morning chek_out"
				);
		$this->emp_info->check_out($check_out);
		}elseif($my_time > '02:30:00' && $my_time < '07:30:01'){
					$check_out = array(
					'check_in_date' =>$date,
					'check_out_time' =>$my_time,
					'check_type'   =>"Morning chek_out"
				);
			$this->emp_info->check_out($check_out);
		}
		elseif($my_time > '07:30:00' && $my_time < '11:30:01'){
					$check_out = array(
					'check_in_date' =>$date,
					'check_out_time' =>$my_time,
					'check_type'   =>"Afternoon chek_out"
				);
			$this->emp_info->check_out($check_out);
		}elseif($my_time > '11:30:01'){
					$check_out = array(
					'check_in_date' =>$date,
					'check_out_time' =>$my_time,
					'check_type'   =>"over_time"
				);
			$this->emp_info->check_out($check_out);
		}
		$this->session->sess_destroy();
		redirect('login');
	}
	
}
