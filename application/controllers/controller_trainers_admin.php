<?php
	class Controller_trainers_admin extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->model_trainers_admin();
			$data['trainers'] = null;
			if($query){
				$data['trainers'] = $query;
			}
			
			$this->load->view("training/view_trainers_admin", $data);
		}
	}
?>