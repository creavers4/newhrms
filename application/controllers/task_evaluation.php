<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Task_evaluation extends CI_Controller {
	public function __construct ()
	{
		parent::__construct();
        if(!$this->session->userdata('username')){
                redirect('login', 'refresh');
         }
		$this->load->model('admin_setting');
        $this->load->model('task_and_evaluation');
        $this->load->model('emp_info');
	}
	
    public function index(){
        $data['dep_id'] = $this->admin_setting->find_dep_id_by_session();

                           foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;

        $data['select_jobe_names']=$this->admin_setting->select_jobe_names($dep_id);



         $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
        $this->load->view('evalutionandtask/task_register',$data);
    }
    public function register_new_task(){
        $this->form_validation->set_rules('taskName','Task Name',"required");
         $this->form_validation->set_rules('taskdiscription','Task description',"required");
          $this->form_validation->set_rules('jobTitle','Job Title',"required");
          if($this->form_validation->run() == False){
              $data['select_jobe_names']=$this->admin_setting->select_jobe_names();
              $this->load->view('evalutionandtask/task_register',$data);
          }else{
              $jobeTitle  = set_value('jobTitle');
			$data['job_id'] = $this->admin_setting->find_jobe_id_by_name($jobeTitle);
             foreach($data['job_id'] as $job_ids):
                            $job_id =   $job_ids->jobe_id; 
                             endforeach;
             $register_new_task = array (
                  'task_subject'         =>set_value('taskName'),
                  'task_discribtion'     =>set_value('taskdiscription'),
                  'job_title'           =>set_value('jobTitle'),
                  'jobe_id'             => $job_id
                  );
                $this->task_and_evaluation->register_new_task($register_new_task);
                 redirect('task_evaluation/register_new_task');
          }
   
    }
        public function assigne_task(){
            $data['dep_id'] = $this->admin_setting->find_dep_id_by_session();

                           foreach($data['dep_id'] as $dep_ids):
                            $dep_id =   $dep_ids->dep_id; 
                             endforeach;
            $data['employees'] =$this->emp_info->view_employees($dep_id);
        $this->form_validation->set_rules('taskTitle','Task Title',"required");
         $this->form_validation->set_rules('taskdiscription','Task description',"required");
          $this->form_validation->set_rules('empName','Employee Name',"required");
          if($this->form_validation->run() == False){
          //  $data['employees'] =$this->emp_info->view_employees();
           $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		    foreach($data['emp_id'] as $emp_ids):
	         $emp_id =   $data['emp_id']->emp_id;
		     endforeach;
			 $data['profies'] =$this->emp_info->profile($emp_id);
              $this->load->view('evalutionandtask/assigne_task',$data);
          }else{
             $Assigne_Task_task = array (
                  'task_title'                  =>set_value('taskTitle'),
                   'task_discription'         =>set_value('taskDiscription'),
                   'emp_id'                    => 13
                  );
                $this->task_and_evaluation->assigne_new_task($Assigne_Task_task);
             redirect('task_evaluation/assigne_task');
               
          }
   
    }
    
    public function view_task(){
        $data['view_my_task']=$this->task_and_evaluation->view_task();
        $this->load->view('employee/employee_account',$data);
    }
    public function select_employee_name_by_job_id($jobe_id){
        $data['view_evaluator']=$this->task_and_evaluation->view_evaluator();
        $data['select_employee_name_by_job_id']=$this->task_and_evaluation->select_employee_name_by_job_id($jobe_id);
        $this->load->view('evalutionandtask/view_employee_name',$data);
    }
    public function select_task_name_by_employee_id($task_id){
        $data['view_evaluator']=$this->task_and_evaluation->view_evaluator();
       // $data['select_task_name_by_job_ids']=$this->task_and_evaluation->select_task_name_by_job_id($jobe_id);
        $data['select_task_name_by_employee_ids']=$this->task_and_evaluation->select_task_name_by_employee_id($task_id);
        $this->load->view('evalutionandtask/view_task_name',$data);

    }
   public function  send_my_task_to_evaluator(){
        $this->form_validation->set_rules('checkbox','checkbox','required');
      if($this->form_validation->run() == False){
           $data['view_my_task']=$this->task_and_evaluation->view_task();
          $this->load->view('employee/employee_account',$data);
      }else{
          $data['find_emp_id_by_user_names'] =$this->emp_info->find_emp_id_by_user_name();
          $emp_id =  $data['find_emp_id_by_user_names']->emp_id;
          // $cheakbox =set_value('checkbox[]');
           //  for($i=0; $i< 3;$i++){
          $insert_task = array(
             'task_id'           =>set_value('checkbox'),
             'crated_date'      => date('y-m-d'),
              'emp_id'           =>  $emp_id
          );
            
     
       $this->task_and_evaluation->send_my_task_to_evaluator($insert_task);
         redirect('employee_info/employee_account');
         // echo set_value('checkbox[]')[1];
       // echo set_value('checkbox')[$i];
         //   echo set_value('checkbox[3]');
           //  echo set_value('checkbox[3]');
           //   echo set_value('checkbox[3]');
  }
   }

function InsertSymbols() {
    $data = array();
    foreach($this->input->post('symbols') as $symb) {
        $data[] = array('symbol_id' => $symb);
    }
    return $this->db->insert_batch('symbols', $data);
}

      

    
    public function view_evaluator($emp_id){
       $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
		endforeach;
			if($emp_id!=$session_id)
         redirect('index');	
         $data['sender'] = $this->emp_info->find_emp_id_by_user_name();
        $data['profies'] =$this->emp_info->profile($emp_id);
          $data['view_evaluator']=$this->task_and_evaluation->view_evaluator();
    //  foreach ( $data['view_evaluator'] as $view_evaluators): 
    //    echo   $view_evaluators->task_id;
   //   endforeach;
          $this->load->view('evalutionandtask/view_evaluator',$data);
    }

    public function evaluted_data($emp_id){
        if( !$this->uri->segment(3) )
            redirect('index');	
         $data['profies'] =$this->emp_info->profile($emp_id);
         $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
			$this->form_validation->set_rules('attendance', 'Attendance', 'required');
			$this->form_validation->set_rules('task_duration', 'Task Duration', 'required');
			$this->form_validation->set_rules('task_quality', 'Task Quality', 'required');
			$this->form_validation->set_rules('protocol', 'Protocol', 'required');
			$this->form_validation->set_rules('social_interaction', "Social Interaction", 'required');
            $this->form_validation->set_rules('languge', "Language", 'required');

			if ($this->form_validation->run()==FALSE){
                 $data['profies'] =$this->emp_info->profile($emp_id);
        foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
                $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
                $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
				$this->load->view('evalutionandtask/evaluter_form',$data);
			}else{
               // $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
	//	foreach($data['emp_id'] as $emp_ids):
	 // $evaluter_id =   $data['emp_id']->emp_id;
	//	endforeach;
    $username =  $this->session->userdata('username');
    $data['evaluter_id'] = $this->emp_info->find_emp_id_by_user_name();

       $attend =  set_value('attendance');
       $task  =  set_value('task_duration');
       $quality=  set_value('task_quality');
       $protocol=  set_value('protocol');
       $languge=  set_value('languge');
       $social_interaction=  set_value('social_interaction');
       $total = $attend + $task + $quality +$protocol + $social_interaction + $languge ;
       $average = $total/6;
       $resualt = $average;
       if($resualt ==5){
           $remark = "Excellent";
       }
      elseif($resualt >=4){
           $remark = "Very Good";
       }elseif($resualt >=3){
           $remark =  "Good";
       }
       else{
           $remark = "bad";
       }

	
	$data_evaluted = array(
							  'attendance'         => set_value('attendance'),
							  'task_duration' 	   => set_value('task_duration'),
							  'task_quality'       => set_value('task_quality'),
							  'protocol'           => set_value('protocol'),
							  'social_interaction' => set_value('social_interaction'),
                              'crated_date'        =>date('y-m-d'),
                              'emp_id'             =>$emp_id,
                              "evaluter_name"      =>$username,
                              "evaluter_id"        => $data['evaluter_id']->emp_id,
                              'language'           => $languge,
                              'total'              => $average,
                              'remark'             =>$remark
					);
				$this->task_and_evaluation->evaluted_data($data_evaluted);
                $this->session->flashdata('key',$emp_id);
                redirect('employee_info/view_employee');
			}

    }
    public function view_evalute($emp_id){
    $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
       $my_role_level= $data['emp_id']->role_level;
	   $dep_id_role =   $data['emp_id']->dep_id;
		endforeach;
        $data['profies'] =$this->emp_info->profile($emp_id);
        foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
            $data['my_dep_ids'] = $this->admin_setting->find_dep_id_by_emp_id($emp_id);
			 foreach($data['my_dep_ids'] as $my_dep_ids1):
	        $my_dep_id =   $my_dep_ids1->dep_id;
		    endforeach;
            $data['view_evalute'] = $this->task_and_evaluation->view_evalute($emp_id);
        $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
        $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
        $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
        
        $query = $this->task_and_evaluation->view_evaluation_id($emp_id);
			if($query){
				$data['evaluation_id'] = $query;
			}
                if($my_role_level == "employee"){
				if($emp_id==$session_id){
        $this->load->view('evalutionandtask/view_evalute',$data);
                  }
                }elseif($my_role_level == "department head"){
				if(($emp_id==$session_id) || ($dep_id_role == $my_dep_id)){
			$data['view_evalute'] = $this->task_and_evaluation->view_evalute($emp_id);
        $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
        $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
		 $this->load->view('evalutionandtask/view_evalute',$data);

			}else{
					redirect('index');
				}
			}elseif($my_role_level == "superadmin"){
			
			 $this->load->view('evalutionandtask/view_evalute',$data);
                
			}
			else{
					redirect('index');
				}
                
    }
    public function message($emp_id){
        if( !$this->uri->segment(3) )
            redirect('index');
			 $data['dep_id'] = $this->admin_setting->find_dep_id_by_session();
			foreach($data['dep_id'] as $dep_ids):
			$dep_id = $dep_ids->dep_id;
			endforeach;
		$data['notificatio'] = $this->emp_info->count_send_document($dep_id);
    $data['unread_message'] =  $this->emp_info->count_unread_message($dep_id);
		$data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();


      $data['sender'] = $emp_id;
     $data['profies'] =$this->emp_info->profile($emp_id);
        $this->load->view('evalutionandtask/message',$data);
    }
    public function send_message($emp_id){
        if( !$this->uri->segment(3) )
            redirect('index');	
           $this->form_validation->set_rules('message', "Message", 'required');
           $this->form_validation->set_rules('subject', "subject", 'required');
			if ($this->form_validation->run()==FALSE){
                $data['profies'] =$this->emp_info->profile($emp_id);
                $data['sender'] = $emp_id;
				$this->load->view('evalutionandtask/message',$data);
			}else{
                $reciver_id= $emp_id;
                 $data['sender'] = $this->emp_info->find_emp_id_by_user_name();
                 foreach($data['sender'] as $sender):
                            $sender_id =  $data['sender']->emp_id; 
                             endforeach;

        $data_message =array(
                 'subject'  => set_value('subject'),
                 'message'  => set_value('message'),
                  'sender_id'  =>$sender_id,
                  'reciver_id' =>$reciver_id,
                  'message_status' =>'unread'
                  );
                  $this->task_and_evaluation->send_message($data_message);
                  redirect('employee_info/view_employee');
                  }
                  
}
public function view_message($emp_id){
   $data['emp_id'] = $this->emp_info->find_emp_id_by_user_name();
		foreach($data['emp_id'] as $emp_ids):
	  $session_id =   $data['emp_id']->emp_id;
		endforeach;
			if($emp_id!=$session_id)
         redirect('index');
    $data['sender'] = $this->emp_info->find_emp_id_by_user_name();
    $data['profies'] =$this->emp_info->profile($emp_id);
    foreach($data['profies'] as $dep_ids):
	        $dep_id =   $dep_ids->dep_id;
		    endforeach;
        $data_update_message_status = array('message_status' =>"read");
    $this->task_and_evaluation->update_message_status($emp_id,$data_update_message_status);
    $data['role_level'] = $this->emp_info->find_emp_role_level_by_user_name();
    $data['notificatio'] = $this->emp_info->count_send_document($dep_id);
    $data['unread_message'] =  $this->emp_info->count_unread_message($emp_id);
    $reciver_id = $emp_id;
    $sender_id = $data['sender']->emp_id;
    $data['view_message'] = $this->task_and_evaluation->view_message($reciver_id,$sender_id);

    $this->load->view('evalutionandtask/view_message',$data);
}

public function view_messageID(){
      $query = $this->task_and_evaluation->view_messageID();
      if($query){
        $data['message'] = $query;
      }
      $this->load->view('employee/employee_account', $data);
    }
}