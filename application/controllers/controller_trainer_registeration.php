<?php
	class Controller_trainer_registeration extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('model_training');
		}
		public function index(){
			$query = $this->model_training->trainings_drop_down();
			if($query){
				$data['trainings'] = $query;
			}
			$this->load->view('training/view_trainer_registeration', $data);

		}
		public function update_status(){
			$update = array('status' 	=> 'contract_completed');
			$this->model_training->update_trainers_status($update);
			redirect('controller_trainers_view');
		}
		public function register_trainers(){
			$this->form_validation->set_rules('trainerId', 'Trainer ID', 'required');
			$this->form_validation->set_rules('fname', 'First Name', 'required');
			$this->form_validation->set_rules('sex', 'Sex', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			$this->form_validation->set_rules('lname', 'Last name', 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('training/view_trainer_registeration');
			}else{
   				$data = array('trainerId'         		=> set_value('trainerId'),
							  'fname'           		=> set_value('fname'),
							  'lname'         			=> set_value('lname'),
							  'sex' 	   				=> set_value('sex'),
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'city' 					=> set_value('city'),
							  'email'           		=> set_value('email'),
							  'training'           		=> set_value('training'),
							  'experience'           	=> set_value('experience'),
							  'specialized_field'       => set_value('specialized_field'),
							  'specific_stay'           => set_value('specific_stay'),
							  'salary'                	=> set_value('salary'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')

					);
				$this->model_training->model_trainer_insert($data);
				
				}
				redirect('controller_trainer_registeration');
			}
			
		}
		

?>