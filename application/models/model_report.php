<?php
	class Model_report extends CI_Model{
        public function attendance_report(){
            $this->db->select("*");
			$this->db->from('attendancetable');
			$attendance_report = $this->db->get();
			return $attendance_report->result();
        }
        public function attendance_enroll_no_2(){
            $this->db->select("*");
			$this->db->from('attendancetable');
            $this->db->where("enroll_number", 2);
			$enroll_no_2 = $this->db->get();
			return $enroll_no_2->result();
        }
        public function attendance_enroll_no_5(){
            $this->db->select("*");
			$this->db->from('attendancetable');
            $this->db->where("enroll_number", 5);
			$enroll_no_2 = $this->db->get();
			return $enroll_no_2->result();
        }
        public function attendance_enroll_no_7(){
            $this->db->select("*");
			$this->db->from('attendancetable');
            $this->db->where("enroll_number", 7);
			$enroll_no_2 = $this->db->get();
			return $enroll_no_2->result();
        }
        public function evaluation_report(){
            $this->db->select("*");
			$this->db->from('evaluation_task');
			$evaluation_report = $this->db->get();
			return $evaluation_report->result();
        }
        public function employee_report(){
            $this->db->select("*");
			$this->db->from('empe_info');
			$employee_report = $this->db->get();
			return $employee_report->result();
        }
        public function document_report(){
            $this->db->select("*");
			$this->db->from('our_document');
			$document_report = $this->db->get();
			return $document_report->result();
        }
        public function trainings_report(){
            $this->db->select("*");
			$this->db->from('training');
			$trainings_report = $this->db->get();
			return $trainings_report->result();
        }
        public function trainees_report(){
            $this->db->select("*");
			$this->db->from('trainee');
			$trainees_report = $this->db->get();
			return $trainees_report->result();
        }
        public function trainers_report(){
            $this->db->select("*");
			$this->db->from('trainer');
			$trainers_report = $this->db->get();
			return $trainers_report->result();
        }
    }
?>