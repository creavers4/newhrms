<?php
	class Model_training extends CI_Model{
		public function model_trainer_insert($data){
			$this->db->insert('trainer', $data);
		}
		public function staff_career_development($data){
			$this->db->insert('staff_career_development', $data);
		}
		public function add_documents($add_documents){
			$this->db->insert('trainer_document', $add_documents);
		}
		public function model_trainee_insert($data){
			$this->db->insert('trainee', $data);
		} 
		public function model_training_insert($data){
			$this->db->insert('training', $data);
		}
		public function model_trainers_view(){
			$this->db->select("*");
			$this->db->from('trainer');
			$query = $this->db->get();
			return $query->result();
		}
		public function model_trainees_admin(){
			$this->db->select("*");
			$this->db->from('trainee');
			$this->db->where('status', 'contract_completed');
			$this->db->order_by('expiration_date', 'DESC');
			$query = $this->db->get();
			return $query->result();
		}
		public function model_trainers_admin(){
			$this->db->select("*");
			$this->db->from('trainer');
			$this->db->where('status', 'contract_completed');
			$this->db->order_by('expiration_date', 'DESC');
			$query = $this->db->get();
			return $query->result();
		}
		public function trainings_view_for_update(){
			$this->db->select("*");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
		}
		public function trainings_view_for_select(){
			$this->db->select("*");
			$this->db->from('training');
			$this->db->where('training', $select);
			$query1 = $this->db->get();
			return $query1->result();
		}
		public function model_trainees_view(){
			$this->db->select("*");
			$this->db->from('trainee');
			$this->db->order_by('training_id','desc');
			$query = $this->db->get();
			return $query->result();
		}
		public function model_trainings_view(){
			$this->db->select("*");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
		}
		public function trainings_drop_down(){
			$this->db->select("training_id, training, specify_schedule, fee");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
		}
		public function trainers_drop_down(){
			$this->db->select("fname, lname, training, specific_stay");
			$this->db->from('trainer');
			$query1 = $this->db->get();
			return $query1->result();
		}
		public function update_status($update){
			$this->db->where('expiration_date', date("Y-m-d"));
			$this->db->update('trainee', $update);
		}
		public function update_trainers_status($update){
			$this->db->where('expiration_date', date("Y-m-d"));
			$this->db->update('trainer', $update);
		}

		public function update_trainings($update,$select){
			$this->db->where('training', $select);
			$this->db->update('training', $update);
		}

		public function trainees_view_for_update(){
			$this->db->select("*");
			$this->db->from('trainee');
			$query = $this->db->get();
			return $query->result();
		}

		public function update_trainees($update,$select){
			$this->db->where('traineeId', $select);
			$this->db->update('trainee', $update);
		}

		public function trainers_view_for_update(){
			$this->db->select("*");
			$this->db->from('trainer');
			$query = $this->db->get();
			return $query->result();
		}

		public function update_trainers($update,$select){
			$this->db->where('trainerId', $select);
			$this->db->update('trainer', $update);
		}
		

	}
?>