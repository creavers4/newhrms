<?php
    class Model_message extends CI_Model{
        public function send_message($data){
            $this->db->insert('message', $data);
        }
        public function view_message($emp_id){
			$this->db->select("*");
			$this->db->from('message');
			$this->db->where('reciver_id', $emp_id);
            $this->db->order_by('message_id', 'desc');
			$query = $this->db->get();
			return $query->result();
		}
        public function view_staff_message(){
			$this->db->select("*");
			$this->db->from('message');
			$this->db->join('empe_info','empe_info.emp_id = message.sender_id');
            $this->db->order_by('message_id', 'desc');
			$query = $this->db->get();
			return $query->result();
		}
        public function update_approval($update, $message_i){
            $this->db->where('message_id', $message_i );
			$this->db->update('message', $update);
        }
        public function employee_id(){
			$this->db->select('*');
			 $this->db->from('evaluation_task');
			 $query = $this->db->get();
			return $query->result();
		}
    }