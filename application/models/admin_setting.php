<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_setting extends CI_Model {	
	public function register_new($data_register_new)
	{
		$this->db->insert('users',$data_register_new);		
	}

	public function select_administretor(){
		$select = $this->db->get('users');
		if($select->num_rows() > 0){
			return $select->result();

		}else{
			return array();
		}
	}
	

	public function create_new_department($data_new_department)
	{
		$this->db->insert('department',$data_new_department);		
	}
	public function create_new_jobe($data_new_jobe)
	{
		$this->db->insert('jobe',$data_new_jobe);		
	}
	public function get_last_inserted_id(){
		$show = $this->db->get('jobe');
     if($show->num_rows() > 0){
       return $this->db->insert_id();
      }else{
       return array();
      }
		
	}
	public function select_dep_names(){
		 $show = $this->db->get('department');
     if($show->num_rows() > 0){
       return $show->result();
      }else{
       return array();
      }
	}
	public function select_jobe_names(){
		$show = $this->db->get('jobe');
		if($show->num_rows() > 0){
			return $show->result();
		}else{
			return array();
		}
	}

	public function select_jobe_info(){
    $this->db->select('*');
		  $this->db->from('jobe');
		  $this->db->join('salary','jobe.jobe_id = salary.jobe_id');
		//  $this->db->join('task','task.task_id = evaluation_task.task_id');
		 // $this->db->where('empe_info.emp_id',$emp_id);
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}

	
	public function find_dep_id_by_name($depName){
		$select_id = $this->db->where('dep_name',$depName)
			                ->select('dep_id')
							->limit(1)
							->get('department');
		if($select_id->num_rows() > 0){
			return $select_id->result();
		}else{
			return array();
		}

	}
	public function find_dep_id_by_emp_id($emp_id){
		$select_id = $this->db->where('emp_id',$emp_id)
			                ->select('dep_id')
							->limit(1)
							->get('empe_info');
		if($select_id->num_rows() > 0){
			return $select_id->result();
		}else{
			return array();
		}

	}
	public function find_dep_id_by_session(){
		$username =$this->session->userdata('username');
		$select_id = $this->db->where('username',$username)
			                ->select('dep_id')
							->limit(1)
							->get('empe_info');
		if($select_id->num_rows() > 0){
			return $select_id->result();
		}else{
			return array();
		}

	}
		public function find_jobe_id_by_name($jobTitle){
		$select_id = $this->db->where('jobe_title',$jobTitle)
			                ->select('jobe_id')
							->limit(1)
							->get('jobe');
		if($select_id->num_rows() > 0){
			return $select_id->result();
		}else{
			return array();
		}

	}
	public function add_documents($add_documents){
		$this->db->insert('our_document',$add_documents);
	}
}
	
	
	
?>
		