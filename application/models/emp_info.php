<?php
  class Emp_info extends CI_Model
  {
		public function check_emp_employee()
		{
			$username = set_value('username');	
			$password = set_value('password');	
			$stuts = '1';
			$gry = $this->db->where('username',$username)
							->where('password',$password)
							->where('emp_status',$stuts)
							->limit(1)
							->get('empe_info');
			if($gry->num_rows()	>	0)
			{
				return $gry->row();	
			}else{
					return array();
			}
				
				
		}
		public function check_employee_is_active()
		{
			$username = set_value('username');	
			$password = set_value('password');	
			$stuts = '0';
			$gry = $this->db->where('username',$username)
							->where('password',$password)
							->where('emp_status',$stuts)
							->limit(1)
							->get('empe_info');
			if($gry->num_rows()	>	0)
			{
				return $gry->row();	
			}else{
					return array();
			}
				
				
		}
  	
public function create_new_employee($creat_new_employee)
		{	
			$this->db->insert('empe_info',$creat_new_employee);		
		}
			public function add_employee_sallary($data_salary){
			$this->db->insert('salary',$data_salary);
		}

    
		public function view_employees($dep_id){
    	
		  $this->db->select('*');
		  $this->db->from('empe_info');
		  $this->db->join('department','department.dep_id = empe_info.dep_id');
	//  $this->db->join('jobe','jobe.dep_id = department.dep_id');
		  $this->db->join('salary','salary.jobe_id = empe_info.jobe_id');

		  if($dep_id == 1){
 $query = $this->db->get();
		}else{
 $query = $this->db->where('empe_info.dep_id',$dep_id)->get();
		}
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}
		public function profile($emp_id){
          $this->db->select('*');
		  $this->db->from('empe_info');
		  $this->db->join('department','department.dep_id = empe_info.dep_id');
		  $this->db->join('jobe','department.dep_id = jobe.dep_id');
		  $query = $this->db->where('empe_info.emp_id',$emp_id)->get();
		  if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
				
		}


    public function find($emp_id)
		{ 
			$code = $this->db->where('emp_id',$emp_id)
							->limit(1)
							->get('empe_info');
			if ($code->num_rows() > 0 )
				{
					return $code->row();
				}else {
					return array();
				}
			}
			public function find_emp_id_by_user_name(){
				 $username = $this->session->userdata("username");
				$code = $this->db->where('username',$username)
							->limit(1)
							->get('empe_info');
			if ($code->num_rows() > 0 )
				{
					return $code->row();
				}else {
					return array();
				}
				
			}
			public function find_emp_role_level_by_user_name(){
				 $username = $this->session->userdata("username");
				$code = $this->db->where('username',$username)
				            ->select('role_level')
							->limit(1)
							->get('empe_info');
			if ($code->num_rows() > 0 )
				{
					return $code->row();
				}else {
					return array();
				}
				
			}

      public function update_employee($emp_id,$data_edit_employee){
			$this->db->where('emp_id',$emp_id)
					->update('empe_info',$data_edit_employee);
      }
			public function disable_emp($emp_id,$data_emp)
		{	
			$this->db->where('emp_id',$emp_id)
			->update('empe_info',$data_emp);
			
		}
		public function active_emp($emp_id,$data_emp){
			$this->db->where('emp_id',$emp_id)
			->update('empe_info',$data_emp);
		}
			public function check()
	{
		$username = set_value('rusername');	
		$password = set_value('rpassword');	
		$stuts = '1';
		$gry = $this->db->where('user_name',$username)
		->where('usr_password',$password)
		->where('stuts',$stuts)
		->limit(1)
		->get('users');
		if($gry->num_rows()	>	0)
		{
			return $gry->row();	
			}else{
			return array();
		}
		
	}
	
	public function check_password_for_change()
		{
			$old_password	= set_value('oldpassword'); 	
			$username = $this->session->userdata('username');
			$query= $this->db->where('username',$username)
							->where('password',$old_password)
							->limit(1)
							->get('empe_info');
			if($query->num_rows()	>	0)
			{
					return $query->row();	
			}else{
					return array();
			}
		}
	
	public function change_eployee_password($new_employee_password)
		{
			$username =$this->session->userdata('username');
			$this->db->where('username',$username)	
					->update('empe_info',$new_employee_password);
		}
		public function check_in($check_in){
			$this->db->insert('attendance1',$check_in);

		}
		public function check_out($check_out){
			$this->db->insert('attendance1',$check_out);

		}
		public function view_my_doceument($emp_id,$login_id){
		$show = $this->db->where('reciver_id',$login_id)->where('emp_id',$emp_id)->get('our_document');
		if($show->num_rows() > 0 ){
			return $show->result();

		}else{
			return array();
		}

    	}
		public function view_send_doceument($dep_id){
		$show = $this->db->where('dep_id',$dep_id)->order_by('document_id','desc')->get('our_document');
		if($show->num_rows() > 0 ){
			return $show->result();

		}else{
			return array();
		}

	}
	function count_send_document($dep_id)
  {
  	$status = 'unread';
  $query = $this->db->where('dep_id',$dep_id)->where('document_status',$status)->get('our_document');
  return $query->num_rows();


}
function count_unread_message($emp_id)
  {
  	$status = 'unread';
  $query = $this->db->where('reciver_id',$emp_id)->where('message_status',$status)->get('message');
  return $query->num_rows();


}
public function update_document_status($dep_id,$data_pdate_document_status){
			$this->db->where('dep_id',$dep_id)
					->update('our_document',$data_pdate_document_status);
		}
public function view_best_worker(){
		 $this->db->select('*');
		  $this->db->from('evaluation_task');
		  $this->db->join('empe_info','evaluation_task.emp_id = empe_info.emp_id');
		  $this->db->where('remark','Good');
		  $this->db->group_by('empe_info.first_name');
	//	  $this->db->order_by('crated_date','desc');
		  $show = $this->db->get();
		if($show->num_rows() > 0 ){
			return $show->result();

		}else{
			return array();
		}


		}
	function count_best_worker()
  {

	   	$this->db->distinct();
	    //$this->db->where('remark','Good');
	    $query = $this->db->query('SELECT DISTINCT emp_id FROM evaluation_task');
       // $query = $this->db->where('remark','Good')->get('evaluation_task');
  return $query->num_rows();
}

  }