<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Attendance extends CI_Model {
 public function view_attendance()
		{ $record_date = date('y-m-d');
		  $this->db->select('*');
		  $this->db->from('attendance');
		  $this->db->join('empe_info','attendance.emp_id = empe_info.emp_id');
      //$this->db->where('attendance.record_date',$record_date);
	      $this->db->group_by('empe_info.first_name');
		  $this->db->order_by('attendance.attend_id','desc');
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}

        public function view_my_attendance($emp_id)
		{ $record_date = date('y-m-d');
		  $this->db->select('*');
		  $this->db->from('attendancetable');
		  $this->db->join('empe_info','attendancetable.enroll_number = empe_info.enroll_number');
      $this->db->where('empe_info.emp_id',$emp_id);
	   // $this->db->group_by('attendancetable.created_dates');
      $this->db->distinct('attendancetable.created_dates');
		 $this->db->order_by('attendancetable.id','desc');
		  
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}
		public function insert_my_attendance_resualt($data_resualt){
			$this->db->insert('attendance1',$data_resualt);
		}
		public function view_new_attendance(){
			$show = $this->db->get('attendance1');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
		public function my_daily_attendance_resualt($data_resualt2){
			$this->db->insert('attendance',$data_resualt2);
		}
		public function update_daily_attendance_resualt($data_resualt2){
			$date = date('y-m-d');
			$this->db->where('record_date',$date)
					->update('attendance',$data_resualt2);
		}
		public function find_mcheck_in_by_date(){
			$date = date('y-m-d');
			$show = $this->db->where('record_date',$date)
			                         ->select('record_date')
                                     ->limit(1)
                                     ->get('attendance');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
		public function find_duty_off_by_date(){
			$date = date('y-m-d');
			$show = $this->db->where('record_date',$date)
			                         ->select('duty_off')
                                     ->limit(1)
                                     ->get('attendance');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
		public function find_m_check_out_by_date(){
			$date = date('y-m-d');
			$show = $this->db->where('record_date',$date)->get('attendance');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
		public function find_A_check_in_by_date(){
			$date = date('y-m-d');
			$show = $this->db->where('record_date',$date)
			                         ->select('afternoon_check_in')
                                     ->limit(1)
                                     ->get('attendance');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
		public function find_A_check_out_by_date(){
			$date = date('y-m-d');
			$show = $this->db->where('record_date',$date)
			                         ->select('afternoon_check_out')
                                     ->limit(1)
                                     ->get('attendance');
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}

		}
    
}