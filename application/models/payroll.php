<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Payroll extends CI_Model{
  public function select_sallary()
		{ 
		  $this->db->select('*');
		  $this->db->from('empe_info');
		  $this->db->join('salary','empe_info.jobe_id = salary.jobe_id');
		  $this->db->order_by('empe_info.emp_id','desc');
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}


  public function select_emp_id($emp_id)
		{ 
		  $this->db->select('*');
		  $this->db->from('empe_info');
		  $this->db->join('salary','empe_info.jobe_id = salary.jobe_id');
			$this->db->where('emp_id',$emp_id);
		  $this->db->order_by('empe_info.emp_id','desc');
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}



		public function find_emp_salary($emp_id)
		{ 
			$show = $this->db->where('emp_id',$emp_id)
							->limit(1)
							->get('empe_info');
			if ($show->num_rows() > 0 )
				{
					return $show->row();
				}else {
					return array();
				}
			}
	  public function update_emp_sallary($salary_id,$data_edit_emp_sallary){
			$this->db->where('salary_id',$salary_id)
					->update('salary',$data_edit_emp_sallary);
      }
			public function update_over_time($emp_id,$data_edit_over_time){ 
			$this->db->where('emp_id',$emp_id)
			       ->update('empe_info',$data_edit_over_time);
			}
			public function  data_insert_emp_over_time($data_insert_emp_over_time){
				$this->db->insert('overtime_record',$data_insert_emp_over_time);
			}
  }