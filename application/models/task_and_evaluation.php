<?php
class task_and_evaluation extends CI_Model{
    public function register_new_task($register_new_task){
        $this->db->insert('task',$register_new_task);
    }
	 public function assigne_new_task($Assigne_Task_task){
        $this->db->insert('assigne_task',$Assigne_Task_task);
    }
    public function view_task(){
        $show = $this->db->get('task');
        if($show->num_rows() > 0){
          return $show->result();
        }else{
            return array();
        }
    }
	public function view_assigned_task(){
    	
		  $this->db->select('*');
		  $this->db->from('assigne_task');
		  $this->db->join('empe_info','assigne_task.emp_id = empe_info.emp_id');
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}

public function select_employee_name_by_job_id($jobe_id){
    	{ 
		  $this->db->select('*');
		  $this->db->from('empe_info');
		  $this->db->join('jobe','empe_info.jobe_id = jobe.jobe_id');
        $this->db->where('jobe.jobe_id',$jobe_id);
	//	$this->db->group_by('task.task_subject');
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}

}
public function select_task_name_by_employee_id($emp_id){
    $this->db->select('*');
		  $this->db->from('evaluation_task');
		  $this->db->join('empe_info','evaluation_task.emp_id = empe_info.emp_id');
		  $this->db->join('task','task.task_id = evaluation_task.task_id');
		  $this->db->where('empe_info.emp_id',$emp_id);
		   
		  $query = $this->db->get();
			if($query->num_rows() > 0 ) {
					return $query->result();
			} else {
					 return array();
			} 
					
		}

		

  public function   send_my_task_to_evaluator($insert_task){
      $this->db->insert('evaluation_task',$insert_task);
  }
  public function view_evaluator()
		{ 
		  $this->db->select('*');
			 $this->db->from('evaluation_task');
			 $this->db->order_by('evaluation_id','desc');
			 $this->db->join('empe_info','empe_info.emp_id = evaluation_task.emp_id');

		    // $this->db->where('evaluation_task.emp_id',$emp_id);
			 $show = $this->db->get();
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}
					
		}


		public function evaluted_data($evaluted_data){
			$this->db->insert('evaluation_task',$evaluted_data);
		}
		public function update_approval($update, $evaluation_id){
            $this->db->where('evaluation_id', $evaluation_id );
			$this->db->update('evaluation_task', $update);
        }
		public function view_evalute($emp_id){
			 $this->db->select('*');
			 $this->db->from('evaluation_task');
			 $this->db->join('empe_info','empe_info.emp_id = evaluation_task.emp_id');
		     $this->db->where('evaluation_task.emp_id',$emp_id);
			 $show = $this->db->get();
			if($show->num_rows() > 0){
				return $show->result();

			}else{
				return array();
			}
		}
		public function send_message($data_message){
			$this->db->insert('message',$data_message);

		}
		public function view_message($reciver_id,$sender_id){
        $show = $this->db->order_by('message_id','desc')->where('reciver_id',$reciver_id)->or_where('sender_id',$sender_id)->get('message');
        if($show->num_rows() > 0){
          return $show->result();
        }else{
            return array();
        }
    }
	function count_message()
  {


     	$status = 'unread';
         $query = $this->db->where('status',$status)->get('messages');
      return $query->num_rows();
}
public function update_message_status($emp_id,$data_update_message_status){
			$this->db->where('reciver_id',$emp_id)
					->update('message',$data_update_message_status);
		}

		public function view_messageID(){
			$this->db->select('*');
			 $this->db->from('message');

			 $query = $this->db->get();
			return $query->result();
		}

		
		public function view_evaluation_id($emp_id){
			$this->db->select('*');
			 $this->db->from('evaluation_task');
			 $this->db->where('emp_id', $emp_id);
			 $query = $this->db->get();
			return $query->result();
		}


	  }

