<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3> <i class="icon-edit-sign"></i>  <i class="fa fa-money" aria-hidden="true"></i>
  sallary  <h3>
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th> Full Name</th>
										<th>Basic Salary</th>
										<th>Over Time</th>
										<th>Other</th>
										<th>Tax</th>
										<th>Add Over Time</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($salariys as $salary) :?>
												<tr>
												<th><?php echo anchor('payrolls/employee_slip/'.$salary->emp_id,$salary->first_name." ".$salary->last_name,['class'=>'btn btn-default btn-xs ']);?></th>
												<th><?php  echo $salary->basic_sallary." "."Birr";?></th>
												<th><?php  echo $salary->over_times." "."Birr";?></th>
												<th><?php  echo $salary->other." "."Birr";?></th>
												<th><?php  echo $salary->tax." "."Birr";?></th>
												<th>  
								<?php if($this->session->userdata('role') == 'admin') : echo  anchor('payrolls/edit_sallary/'.$salary->emp_id,'Add Over Time ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want to Add Over Time This user ? \')'
								])  ?>
								<?php endif;?>
								
								
								

												</th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>