<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i>   Attendances <center><?php echo "Today Employee Attendance " . date('y-m-d') . "<br>" ?> <h4>
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="attendance">
										<thead>
									<tr>
                                        <th>Full Name</th>
                                        <th> M check in</th>
										<th> M check_out</th>
										<th> A check in</th>
                                        <th> A check out</th>
										<th> duty_on</th>
                                        <th> duty_off</th>
                                        <th> Remark</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($view_attendances as $attendance) :?>
												<tr>
                                                <th><?php echo anchor('attendances/my_attendance/'.$attendance->emp_id,$attendance->first_name." ".$attendance->last_name,['class'=>'btn btn-danger btn-xs ']);?></th>
												<th><?php  echo $attendance->morning_check_in;?></th>
												<th><?php  echo $attendance->morning_check_out;?></th>
												<th><?php  echo $attendance->afternoon_check_in;?></th>
												
												<th><?php  echo $attendance->afternoon_check_out;?></th>
												<th><?php  echo $attendance->duty_on;?></th>
                                                <th><?php  echo $attendance->duty_off;?></th>
                                                <th><?php  echo $attendance->remark;?></th>
												<th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#attendance').DataTable();
									
			});
							</script>
</div>
</body>
</html>