<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<?php $this->load->view('layout/sidebar');?>

	<div class="row">
<div class="col-md-8 sidebar">
					<div class="panel panel-default">
						<div class="panel-heading">
                        <?php foreach ($my_attendances as $attendance) :?>
						<?php if($attendance):?>
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-user"></i>   <?php  echo $attendance->first_name." ".$attendance->last_name;?>  Attendances <h4>
					<?php break;?>
					<?php endif;?>
						<?php endforeach; ?>
							</div>
							<div class="panel-body">
							<p id="date_filter">
    <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
    <span id="date-label-to" class="date-label">To:<input class="date_range_filter date" type="text" id="datepicker_to" />
</p>
							
									<table class="table table-striped table-hover" id="datatable">
									
										<thead>
									 <tr>
        <th>Date</th>
        <th>M Check In</th>
		 <th>M Check Out</th>
		  <th>A Check In</th>
		   <th>A check Out</th>
		 <!--  <th>Duty On</th>
		   <th>Duty Off</th>
		  <th> Remark</th>-->
									</tr>
										</thead>
										<tbody>
										
											 <?php foreach ($my_attendances as $attendance) :?>
<?php
											// $duty_on = $attendance->duty_on ;
                                           //   $duty_off = $attendance->duty_off ;
											  ?>
												
										
											<tr>
												<td><?php echo  $attendance->created_dates;?></td>

												
											<td>	<?php 

                                                  


                                                    if($attendance->log_time >='08:30:00' && $attendance->log_time <='12:30:00'){
                                                    echo $attendance->log_time;
                                                       
                                                    }?>
													
													</td>
                                                
										<td>	<?php	 if($attendance->log_time >='12:30:01' && $attendance->log_time <='13:30:00'){
                                                    echo $attendance->log_time;
													}?></td>
												<td>	<?php	 if($attendance->log_time >='13:30:01' && $attendance->log_time <='17:30:00'){
                                                    echo $attendance->log_time;
													}?></td>

													<td>	<?php	 if($attendance->log_time >='17:30:01'){
                                                    echo $attendance->log_time;
													}?></td>
												
											
												</tr> 
												
												<?php endforeach; ?>
											
										</tbody>
									</table>
										
									 
									</div>
							</div>
							<?php $this->load->view('layout/footer')?>
							</div>
							</div>
	
	<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#datatable').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });




  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);
} );	
	</script>
</div>
</body>
</html>