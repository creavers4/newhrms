<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
	
<div class="col-sm-4 col-md-3 sidebar">
    <div class="list-group">
        <span href="#" class="list-group-item active">
            Jobe Name
            <span class="pull-right" id="slide-submenu">
              
            </span>
        </span>
		<?php foreach ($view_evaluator as $view_evaluators) :?>
        <a>
         
<?=  anchor('task_evaluation/select_employee_name_by_job_id/'.$view_evaluators->jobe_id,$view_evaluators->jobe_title,['class'=>"list-group-item"]) ?>

        </a>
			<?php endforeach;?>
    </div>        
	</div>
	<div class="col-sm-4 col-md-3 sidebar">
    <div class="list-group">
        <span href="#" class="list-group-item active">
            Employee Name
            <span class="pull-right" id="slide-submenu">
              
            </span>
        </span>
	<?php foreach ($select_employee_name_by_job_id as $select_employee_name_by_job_ids) :?>
        <a>
         
<?=  anchor('task_evaluation/select_task_name_by_employee_id/'.$select_employee_name_by_job_ids->emp_id,$select_employee_name_by_job_ids->username,['class'=>"list-group-item"]) ?>

        </a>
			<?php endforeach;?>
    </div>        
	</div>


    
	<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i>  Task Evaluation <h4>
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th>employee Name</th>
										<th>crated_date</th>
										<th>evaluate</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($select_employee_name_by_job_ids as $view_evaluators) :?>
												<tr>

												<th><?php  echo $select_employee_name_by_job_ids->first_name;?></th>
												
												
												<th><?php  echo $select_employee_name_by_job_ids->last_name;?></th>
												<th>
												<?php echo  anchor('creat_new_admin/Edit_employee/'.$select_employee_name_by_job_ids->emp_id,'Evaluate ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Edit This user ? \')'
								])  ?>
											</th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							</div>
							</div>
                        
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>