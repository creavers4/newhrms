<html>
<?php $this->load->view('layout/header');?>
<?php $this->load->view('layout/navigation');?>
<body>

<?php $this->load->view('layout/sidebar');?>

<div class="row">
<div class="col-md-8">
<div class="panel panel-default">
<?php foreach($profies as $profile):?>
						<div class="panel-heading">
                   <h4><?php echo $profile->first_name." ".$profile->last_name; ?> </h4>
                        </div>

<?php echo form_open_multipart("task_evaluation/evaluted_data/".$profile->emp_id);  ?>
<?php endforeach; ?>
  	<input type="number" name="attendance" step="any" min="1" max="5" value="<?php echo set_value('attendance'); ?>"> capacity of interaction with other Department or Company's staff (between 1 and 5):
  	<br><br>
  	<input type="number" name="task_duration" step="any" min="1" max="5" value="<?php echo set_value('task_duration'); ?>">Behavioral attitude (between 1 and 5):
  	<br><br>
  	<input type="number" name="task_quality" step="any" min="1" max="5" value="<?php echo set_value('task_quality'); ?>">Ability to innovate and make suggestions (between 1 and 5):
  	<br><br>
	<input type="number" name="social_interaction" step="any"  min="1" max="5" value="<?php echo set_value('social_interaction'); ?>">Capacity of holding confidential information (between 1 and 5):
  	<br><br>
	<input type="number" name="protocol" step="any" min="1" max="5" value="<?php echo set_value('protocol'); ?>">Capacity of meeting deadlines (between 1 and 5):
	<br><br>
	<input type="number" name="languge" step="any" min="1" max="5" value="<?php echo set_value('languge'); ?>">Languages.  (between 1 and 5):
	<br><br>
	<input type='submit' name='submit' value='Submit'>
	<?php echo validation_errors();?>
	<?php echo form_close(); ?>
</div>
<?php $this->load->view('layout/footer')?>
</div>
</div>
</body>
</html>