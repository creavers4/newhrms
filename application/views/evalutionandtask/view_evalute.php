<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<?php $this->load->view('layout/sidebar');?>
	<div class="row">
<div class="col-md-8 sidebar">
					<div class="panel panel-default">
						<div class="panel-heading">
                      
              <h4>   Evalution 
			  <?php foreach($view_evalute as $view_evalutes):?>
			  <?php echo  $view_evalutes->first_name." ".$view_evalutes->last_name; ?>
			  <?php if($view_evalutes->username)
			  break;
			  ?>
               
			  <?php endforeach;?>
			  </h4>
               

</div>
<div class="panel_body">
							<br>

							

							<?php echo form_open_multipart('controller_evaluation_approval/update_approval') ?>
    <div class="radio">
	<div class="col-xs-3">
        <label for="sel1">Evaluation ID </label>
          <select class="form-control" name="evaluation_id" value="<?php echo set_value('evaluation_id'); ?>" >
          <option value="none">-- Select ID --</option>
          <?php foreach($evaluation_id as $row){ ?>
          <option value='<?= $row->evaluation_id; ?>'><?= $row->evaluation_id; ?></option>
          <?php } ?>
       
          </select>
          <br>
          </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="approval" value="<?php echo "accepted"; ?>" checked="checked" />Accept&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="approval" value="<?php echo "declined"; ?>" />Decline
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="select" value="select">
		</div>
		<?php echo form_close(); ?>
		<br><br>
<table class="table table-striped table-hover" id="tableevalute">

<thead>
<tr>
<th>Evaluation ID</th>
<th>date</th>
<th>Evaluator</th>
<th>Interaction</th>
<th>Behavior</th>
<th>Making suggestions</th>
<th>Protecting confidential information</th>
<th>Meeting deadlines</th>
<th>Languages</th>
<th>Total</th>
<th>Approval</th>
</tr>
</thead>

<tbody>
<?php foreach($view_evalute as $view_evalutes):?>
<tr>
<th><?php echo $view_evalutes->evaluation_id;?></th>
<th><?php echo $view_evalutes->crated_date;?></th>
<th><?php echo $view_evalutes->evaluter_name;?></th>
<th><?php echo $view_evalutes->attendance;?></th>
<th><?php echo $view_evalutes->task_duration;?></th>
<th><?php echo $view_evalutes->task_quality;?></th>
<th><?php echo $view_evalutes->protocol;?></th>
<th><?php echo $view_evalutes->social_interaction;?></th>
<th><?php echo $view_evalutes->language;?></th>
<th><?php echo $view_evalutes->total;?></th>
<th><?php echo $view_evalutes->approval;?></th>

</tr>
<?php endforeach;?>
</tbody>


</div>

</div>
</div>
<?php $this->load->view('layout/footer')?>


<script>
		$(document).ready(function(){
			$('#tableevalute').DataTable();
									
			});
							</script>
							</body>
</html>