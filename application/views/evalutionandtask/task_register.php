<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('layout/header')?>
<body>
<?php $this->load->view('layout/navigation')?>
    <div class="container">
       <hr>
        <div class="row">
             <div class="panel panel-default">
                <div class="panel-heading">
                 <h3> <i class="fa fa-book"></i> : Creat New Task </h3> 
                    </div>
                    <div class="panel-body" width="100px">
            
                            <?= validation_errors() ?>
                                <hr>
                            <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
                            <div class="col-md-12">
                            <?php echo form_open_multipart('task_evaluation/register_new_task') ?>
							<div class="col-md-2">
							</div>
                            <div class="col-md-8">
                            <div class="form-group">
                                    <label for="Task">Task  Name : </label>
                                    <input type="text" class="form-control"placeholder ="Enter Task  Name" name="taskName" value="<?php echo set_value('taskName') ?>">
                                </div>
                                 <div class="form-group">
                                    <label for="Task">Task  Discription : </label>
                                    <input type="text" class="form-control"placeholder ="Enter Task  Discription" name="taskdiscription" value="<?php echo set_value('taskdiscription') ?>">
                                </div>
                                 <div class="form-group">
                                    <label for="Jobe Name">Job: </label>
                                    <div class="input-group">
							<div class="input-group-addon">Job</div>
							<select class="form-control" name="jobTitle" <?php echo set_value('jobTitle'); ?>>
                            <?php  foreach($select_jobe_names as $select_job_title) :?>
                            <option><?php  echo $select_job_title->jobe_title; ?> </option>
                             <?php endforeach;?>
                             </select>
                            </div>
                                </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="form-group">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    </div>
                                <div class="col-md-3">
                                    <?=  anchor('employee_info/view_employee','Cancel',['class'=>'btn  btn-default']) ?>
                                </div>
                                </div>
                            <?= form_close() ?>
                            </div>
                            <div class="col-md-3"></div>    
                        </div>  
            </div>  
            </div>
           <hr>
           <?php $this->load->view('layout/footer')?>
</body>
</html>