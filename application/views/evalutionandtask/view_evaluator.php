<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>


<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h4> &nbsp;&nbsp;Evaluation </h4>
		<?php //echo anchor('creat_new_admin/add_documents','Add New Document',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="report">
									&nbsp;&nbsp;&nbsp;<a href="http://localhost/NewHrms/controller_message">Send Personal Message To Staff</a> | 
<a href="http://localhost/NewHrms/controller_message_view">View Sent Message For Staff</a><br><br>
										<thead>
									<tr>
                  <th><font color="red">Staff ID</font></th>
										<th>date</th>

<th>Staff Name</th>
<th>Evaluter Name</th>
<th>Evaluation ID</th>
<th>Interaction</th>
<th>Behavior</th>
<th>Making Suggestions</th>
<th>Protecting confidential information</th>
<th>Meeting deadlines</th>
<th>Languages</th>
<th>Total</th>
<th>Approval</th>
									</tr>
										</thead>
						<tbody>
                        <?php foreach($view_evaluator as $view_evalutes):?>
<tbody>
<tr>
<th><font color="red"><?php echo $view_evalutes->emp_id;?></font></th>
<th><?php echo $view_evalutes->crated_date;?></th>
<?php foreach($profies as $profile):?>

<th><?php echo anchor('task_evaluation/view_evalute/'.$view_evalutes->emp_id,$view_evalutes->username);?></th>
<?php if($profile->emp_id)
break;
?>
<?php ?>
 <?php endforeach;?>
 
<th><?php echo anchor('task_evaluation/message/'.$view_evalutes->evaluter_id,$view_evalutes->evaluter_name);?></th>
<th><?php echo $view_evalutes->evaluation_id;?></th>
<th><?php echo $view_evalutes->attendance;?></th>
<th><?php echo $view_evalutes->task_duration;?></th>
<th><?php echo $view_evalutes->task_quality;?></th>
<th><?php echo $view_evalutes->protocol;?></th>
<th><?php echo $view_evalutes->social_interaction;?></th>
<th><?php echo $view_evalutes->language;?></th>
<th><?php echo $view_evalutes->total;?></th>
<th><?php echo $view_evalutes->approval;?></th>

</tbody>
<?php endforeach;?>
		  				</tbody>
						</table>
									</div>
							</div>
							</div>				
	</div>
</body>
<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#report').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });





} );
  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);	
	</script>
</html>