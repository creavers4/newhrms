<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" " <?php ?>" >CREAVERS</a>
    
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
      <ul class="nav navbar-nav">
      
       <?php if($this->session->userdata('username')): ?>
       <?php if($this->session->userdata('role') =="employee") : ?>
        <li class="active"><?php echo anchor('employee_info/employee_account','My Account');?>  <span class="sr-only">(current)</span></a></li>
<?php endif?>
<?php endif?>
<?php if($this->session->userdata('role') =="admin") : ?>
  <li><?php echo anchor('employee_info/view_employee','Home') ?></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setings <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <?php if($this->session->userdata('role_level') =="superadmin"): ?>
          <li><?php echo anchor('creat_new_admin/create_new_department','Department') ?></li>
          <?php endif;?>
          <?php if($this->session->userdata('role_level') =="department head"): ?>
          <li><?php echo anchor('creat_new_admin/create_new_jobe','Job') ?></li>
           <?php endif;?>
        <!--   <li><?php echo anchor('employee_info/view_employee','Employee') ?></li> -->
            <li role="separator" class="divider"></li>
           <!--  <li><?php echo anchor('creat_new_admin/add_documents/','Document');?></li> -->
             <li><?php echo anchor('employee_info/change_password','Change Password');?></li>
            
          </ul>

        </li>
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Payroll <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <li><?php echo anchor('payrolls','Sallary') ?></li>
            <li role="separator" class="divider"></li>
          </ul>
        </li>
          <li><?php echo anchor('attendances','Attendance');?></li>
<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Evaluation <span class="caret"></span></a>
          <ul class="dropdown-menu">
           <?php if($this->session->userdata('role_level') =="department head"): ?>
           <li><?php echo anchor('task_evaluation','Add New Task');?></li>
            <li><?php echo anchor('task_evaluation/assigne_task','Assigne Task');?></li>
<?php endif;?>
 <?php foreach($profies as $profile):?>
 <?php if($this->session->userdata('role_level') =="superadmin"){ ?>
           <li><?php echo anchor('task_evaluation/view_evaluator/'.$profile->emp_id,'Evaluation');?></li>
           
      <?php     if($profile->emp_id){
        break;
      }?>

 <?php } ?>
 <?php endforeach;?>
          </ul>
          <?php if($this->session->userdata('role_level') =="department head"): ?>
          <li><?php echo anchor('controller_training_admin','Training');?></li>
          <?php endif;?>
          <?php if($this->session->userdata('role_level') =="superadmin") : ?>
          
          <li><?php echo anchor('controller_report','Generate Report');?></li>
          <li><?php echo anchor('http://localhost/Performance/home.html','Employee Performance');?></li>
          <li><?php echo anchor('controller_training','Training');?></li>
          <?php endif?>
        </li>
<?php endif;?>
 <?php if($this->session->userdata('role') =="employee") : ?>
     
      <?php // foreach($find_emp_id_by_user_names as $find_emp_id_by_user_name):?>

      <li> <?php // echo anchor('payrolls/employee_slip/'.$find_emp_id_by_user_name->emp_id,'Slip') ?> </li>
      <?php //endforeach;?>
       <?php endif?>

      </ul>
      <ul class="nav navbar-nav navbar-right">
          <?php if ($this->session->userdata('username')): ?>
         <?php if($this->session->userdata('role') =="employee") : ?>

  <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <li><?php echo anchor('employee_info/add_my_document/'.$emp_id=24,'Add Document') ?></li>
            <li><?php echo anchor('employee_info/change_password','Change Password',['class'=>"list-group-item"]) ?></li>
          </ul>
        </li>



      
       <?php endif?>
			<li><?php echo ('<a>'.'Welcome '.$this->session->userdata('username').'</a>'); ?></li>
			 <li ><?php echo anchor('login/logout','Logout') ?></li>
      <?php endif?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>