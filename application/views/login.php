<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('layout/header') ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<br><br><br><br><br><br>
    <div class="container">

       <hr>
        <div class="row">
             <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
						
							<h3>Login  <i class="fa fa-user"></i> :</h3> 
                    </div>
                    <div class="panel-body" width="100px">
						<div class="col-md-12">
							<?php echo validation_errors() ?>
								<hr>
							<div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
							<div class="col-md-6">
							<?= form_open('login') ?>
								<div class="form-group">
									<label for="username">User Name : </label>
									<input type="text" class="form-control" name="username" >
								</div>
								<div class="form-group">
									<label for="password">Password : </label>
									<input type="password" class="form-control" name="password" >
								</div>
								<div class="form-group">
								<div class="col-md-2"></div>
								<div class="col-md-7">
									<button type="submit" class="btn btn-success">Login</button>
									<?=  anchor(base_url(),'Cancel',['class'=>'btn']) ?>
			
									</div>
								</div>
							<?= form_close() ?>
							</div>
							<div class="col-md-3"></div>	
						</div>  
						
                    </div>
					
					
                </div>

            </div>  
			 <hr>

        </div>
        </div>
          <hr>
		  <br><br>
        <?php $this->load->view('layout/footer')?>
        
</body>

</html>
