<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of Reports</h3>
			
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										
										<tbody>
												<tr>
												<th><a href="controller_attendance_enroll_no_2">Enroll Number 2</a></th>
												<th><a href="controller_attendance_enroll_no_5">Enroll Number 5</a></th>
												<th><a href="controller_attendance_enroll_no_7">Enroll Number 7</a></th>
							                   	</tr> 
										</tbody>
									</table>
									</div>
							</div>
							<?php $this->load->view('layout/footer')?>
							</div>
							
	</div>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
    </script>
</div>
</body>
</html>