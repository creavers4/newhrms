<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i> 

		<?php //echo anchor('creat_new_admin/add_documents','Add New Document',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="report">
										<thead>
									<tr>
										<th>Trainer ID</th>
										<th>Name</th>
										<th>Sex</th>
										<th>Age</th>
                                        <th>Phone</th>
                                        <th>Email</th>
										<th>City</th>
										<th>Training</th>
										<th>Experience</th>
                                        <th>Period</th>
                                        <th>Specific Period</th>
										
									</tr>
										</thead>
						<tbody>
                        <?php foreach($trainers as $row){ ?>
                        <tr>
                        <td><?= $row->trainerId; ?> </td>
                        <td><?= $row->fname; ?> <?= $row->lname; ?> </td>
                        <td><?= $row->sex; ?></td>
                        <td><?= $row->age; ?></td>
                        <td><?= $row->phone; ?></td>
                        <td><?= $row->email; ?></td>
                        <td><?= $row->city; ?></td>
                        <td><?= $row->training; ?></td>
                        <td><?= $row->experience; ?></td>
                        <td><?= $row->stay; ?></td>
                        <td><?= $row->specific_stay; ?></td>
                        
                        </tr>
                        <?php } ?>
		  				</tbody>
                          &nbsp;&nbsp;&nbsp;<h4><a href="controller_training_list">Return</a></h4>
						</table>
									</div>
							</div>
							</div>				
	</div>
<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#report').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });





} );
  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);	
	</script>
</html>