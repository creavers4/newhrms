<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of Reports</h3>
			
							</div>
							<div class="panel-body">
							<h3>
												<a href="controller_trainings_report">Trainings</a><br></br>
												<a href="controller_trainers_report">Trainers</a><br></br>
												<a href="controller_trainees_report">Trainees</a><br></br>
							                   	</h3>
									</div>
							</div>
							<?php $this->load->view('layout/footer')?>
							</div>
							
	</div>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
    </script>
</div>
</body>
</html>