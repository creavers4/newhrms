<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i> 

		<?php //echo anchor('creat_new_admin/add_documents','Add New Document',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="report">
										<thead>
									<tr>
										<th>Enroll Number</th>
										<th>Log Date</th>
										<th>Morning Checkin</th>
										<th>Morning Checkout</th>
                                        <th>Afternoon Checkin</th>
										<th>Afternoon Checkout</th>
									</tr>
										</thead>
						<tbody>
                        <?php foreach($attendance as $row){ ?>
                        <tr>
                        <td><?= $row->enroll_number; ?> </td>
                        <td><?= $row->log_date; ?> </td>
                        <td>
                        <?php 
                        if($row->log_time >='08:30:00' && $row->log_time <='12:30:00'){
                        echo $row->log_time;
                        }?>
                        </td>
                        <td>
                        <?php 
                        if($row->log_time >='12:30:01' && $row->log_time <='13:30:00'){
                        echo $row->log_time;
                        }?>
                        </td>
                        <td>
                        <?php 
                        if($row->log_time >='13:30:01' && $row->log_time <='17:30:00'){
                        echo $row->log_time;
                        }?>
                        </td>
                        <td>
                        </td>
                        </tr>
                        <?php } ?>
		  				</tbody>
						</table>
									</div>
							</div>
							</div>				
	</div>
<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#report').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });





} );
  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);	
	</script>
</html>