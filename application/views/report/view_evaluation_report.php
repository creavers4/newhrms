<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i> 

		<?php //echo anchor('creat_new_admin/add_documents','Add New Document',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="report">
										<thead>
									<tr>
										<th>Employee ID</th>
										<th>Evaluation Date</th>
										<th>Social Interaction</th>
										<th>Behavioral Attitude</th>
                                        <th>Ability To Make Suggestions</th>
										<th>Capacity of holding confidential data</th>
                                        <th>Capacity of meeting deadlines</th>
                                        <th>Language</th>
                                        <th>Evaluator</th>
                                        <th>Total</th>
                                        <th>Remark</th>
									</tr>
										</thead>
						<tbody>
                        <?php foreach($evaluation as $row){ ?>
                        <tr>
                        <td><?= $row->emp_id; ?> </td>
                        <td><?= $row->crated_date; ?> </td>
                        <td><?= $row->attendance; ?></td>
                        <td><?= $row->task_duration; ?></td>
                        <td><?= $row->task_quality; ?></td>
                        <td><?= $row->social_interaction; ?></td>
                        <td><?= $row->protocol; ?></td>
                        <td><?= $row->language; ?></td>
                        <td><?= $row->evaluter_name; ?></td>
                        <td><?= $row->total; ?></td>
                        <td><?= $row->remark; ?></td>
                        </tr>
                        <?php } ?>
		  				</tbody>
						</table>
									</div>
							</div>
							</div>				
	</div>
<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#report').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });





} );
  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);	
	</script>
</html>