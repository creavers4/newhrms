<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
  <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3> <i class="icon-edit-sign"></i>  <i class="fa fa-desktop" aria-hidden="true"></i> Trainers Sign Up  <h3>
              </div>
              <div class="panel-body">

<?php echo validation_errors();?>
<?php echo form_open_multipart('controller_trainer_registeration/register_trainers'); ?>

<div class="col-xs-8">
	<p id="date_filter">
  <span id="date-label-from" class="date-label">Contract Starting Date: </span><input class="date_range_filter date" type="date" name="starting_date" />
  <span id="date-label-from" class="date-label">Contract Expiry Date: </span><input class="date_range_filter date" type="date" name="expiration_date" />
</p>
	<script>
		$(document).ready(function() {
$(function() {
  var oTable = $('#datatable').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });




  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);
} );	
	</script>
  

    <label for="trainerId">Trainer ID  </label>
    <input type="text" class="form-control " name="trainerId" value="<?php echo set_value('trainerId') ?>">
    <br>
    </div>
  <div class="col-xs-6">
    <label for="fName">First name  </label>
    <input type="text" class="form-control" name="fname" value="<?php echo set_value('fname') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="lname">Last name  </label>
    <input type="text" class="form-control" name="lname" value="<?php echo set_value('lname') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="sex">Sex  </label>
    <div class="radio">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "Male"; ?>" checked="checked" />Male &nbsp;&nbsp;&nbsp;<br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "female"; ?>" />Female
      </div>
      <br>
      </div>
      <div class="col-xs-6">
      <label for="age">Age  </label>
    <input type="number" class="form-control" name="age" placeholder="20 - 60" value="<?php echo set_value('age') ?>" min='20' max='60'>
    <br>
    </div>
      <div class="col-xs-6">
    <label for="username">Phone  </label>
    <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="username">Email  </label>
    <input type="text" class="form-control" placeholder="eg. contact@email.com" name="email" value="<?php echo set_value('email') ?>">
    <br>
    </div>
    <div class="col-xs-6">
        <label for="sel1">Training </label>
          <select class="form-control" name="training" value="<?php echo set_value('training'); ?>" >
          <option value="none">-- Select Trainings --</option>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->training; ?>'><?= $row->training; ?> : <?= $row->specify_schedule; ?></option>
          <?php } ?>
       
          </select>
          <br>
          </div>
          
    <div class="col-xs-6">
    <label for="specific_stay">Period </label>
    <input type="text" class="form-control" name="specific_stay" value="<?php echo set_value('specific_stay') ?>">
    <br>
  </div>
  
    
    <div class="col-xs-6">

    <label for="username">City  </label>
    <select class="form-control" name="city" value="<?php echo set_value('city'); ?>" >
          <option value="none">-- Select City --</option>
          <option value="Addis Ababa">Addis Ababa</option>
          <option value="Other">Other</option>
       
          </select>
    <br>
    </div>
    
    <div class="col-xs-6">
    <label for="experience">Work Experience  </label>
    <input type="number" class="form-control" name="experience" value="<?php echo set_value('experience') ?>" min='0' max='50'>
    <br>
    </div>
    <div class="col-xs-6">
    <label for="experience">Specialized Field  </label>
    <input type="text" class="form-control" name="specialized_field" value="<?php echo set_value('specialized_field') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="experience">Fee  </label>
    <input type="text" class="form-control" name="salary" value="<?php echo set_value('salary') ?>">
    </div>
    <div class="col-xs-6">

    <label for="username">Status</label>
    <select class="form-control" name="status" value="<?php echo set_value('status'); ?>" >
          <option value="none">-- Select Trainer Status --</option>
          <option value="on_contract">On Contract</option>
          <option value="available">Available</option>
          <option value="contract_completed">Contract Completed</option>
          <option value="unavailable">Unavailable</option>
       
          </select>
    <br>
    </div>
  </div>
    <br>
  <center><input type='submit' name='submit' value='Sign Up'>
    <h4><a href="controller_training">Return</a></h4>
    </center><br>
<br>
<?php echo form_close(); ?>

</div>
</div>
<?php $this->load->view('layout/footer')?>
              </div>
              
              </div>
  
  <script>
    $(document).ready(function(){
      $('#tableemployess').DataTable();
                  
      });
              </script>
</div>
</body>
</html>