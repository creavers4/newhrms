<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
  <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3> <i class="icon-edit-sign"></i>  <i class="fa fa-desktop" aria-hidden="true"></i> Update Trainings<h3>
              </div>
              <div class="panel-body">

<?php echo validation_errors();?>
<?php echo form_open_multipart('controller_trainings_update/update_trainings'); ?>
<div class="col-xs-6">
        <label for="sel1">Select Training</label>
          <select class="form-control" name="select_training" value="<?php echo set_value('select_training'); ?>" >
          <option value="none">-- Select Trainings --</option>
          <?php foreach($trainings_update as $row){ ?>
          <option value='<?= $row->training; ?>'><?= $row->training; ?> for <?= $row->specify_schedule; ?></option>
          <?php } ?>
          </select>
          </div>
          <br><br><br><br>
          <hr>
    <div class="col-xs-6">

    <label for="trainerId">Period </label>
      <input type="text" class="form-control" name="specify_schedule" value="<?php echo set_value('specify_schedule') ?>">
      <br>
      </div>
      <div class="col-xs-6">

    <label for="trainerId">Fee </label>
      <input type="text" class="form-control" name="fee" value="<?php echo set_value('fee') ?>">
      <br>
      </div>
      
      </div>
    <center><input type='submit' name='submit' value='Update'>
    <h4><a href="controller_training">Return</a></h4>
    </center><br>
    <br>
    </div>
<?php echo form_close(); ?>

</div>
</div>
<?php $this->load->view('layout/footer')?>
              </div>
              
              </div>
  
  <script>
    $(document).ready(function(){
      $('#tableemployess').DataTable();
                  
      });
              </script>
</div>
</body>
</html>