<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
						<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of trainers</h3>
							
						</div>
								<div class="panel-body">
    <h4><a href="controller_training_admin">Return</a></h4>
								
								<table class="table table-striped table-hover" id="view_trainers">
									<thead>
										<tr>
											<th>Trainer Id</th>
											<th>Name</th>
											<th>Phone</th>
											<th>Email</th>
											<th>Training</th>
											<th>Experience</th>
											<th>Specialized Field</th>
											<th>Period</th>
											<th>Salary</th>
											<th>Status</th>
											<th>Contrcat Starting Date</th>
											<th>Contract Expiration Date</th>
										</tr>
										


									</thead>
									<tbody>
									<?php foreach($trainers as $row){ ?>
										<tr>
										
											<?php if($row->status == 'contract_completed'){ ?>
										
											<th><font color = " "><?= $row->trainerId; ?></th>
											<th><font color = " "><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = " "><?=$row->phone; ?></th>
											<th><font color = " "><?=$row->email; ?></th>
											<th><font color = " "><?=$row->training; ?></th>
											<th><font color = " "><?=$row->experience; ?></th>
											<th><font color = " "><?=$row->specialized_field; ?></th>
											<th><font color = " "><?=$row->specific_stay; ?></th>
											<th><font color = " "><?=$row->salary; ?></th>
											<th><font color = "red"><?=$row->status; ?></th>
											<th><font color = " "><?=$row->starting_date; ?></th>
											<th><font color = " "><?=$row->expiration_date; ?></th>
											<?php } ?>
											
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>
								</div>
					</div>
	</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#view_trainers').DataTable();
									
			});
	</script>
</div>
</body>
</html>