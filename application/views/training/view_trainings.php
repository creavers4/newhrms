<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
						<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of trainings</h3>
							
						</div>
								<div class="panel-body">
    <h4><a href="controller_training">Return</a></h4>
								<table class="table table-striped table-hover" id="view_trainings">
									<thead>
										<tr>
											<th>Training ID</th>
											<th>Training</th>
											<th>Vendor</th>
											<th>Period</th>
											<th>Fee</th>
										</tr>
										
									</thead>
									<tbody>
									<?php foreach($trainings as $row){ ?>
										<tr>
											<th><?=$row->training_id; ?></th>
											<th><?=$row->training; ?></th>
											<th><?=$row->vendor; ?></th>
											<th><?=$row->specify_schedule; ?></th>
											<th><?=$row->fee; ?></th>
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>
								</div>
					</div>
	</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#view_trainings').DataTable();
									
			});
	</script>
</div>
</body>
</html>