<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of trainees</h3>
							
						</div>
								<div class="panel-body">
<h4><a href="controller_training_admin">Return</a></h4>
								<table class="table table-striped table-hover" id="view_trainees">
									<thead>
										<tr>
											<th>Trainee Id</th>
											<th>Name</th>
											<th>Sex</th>
											<th>Age</th>
											<th>Phone</th>
											<th>Email</th>
											<th>City</th>
											<th>Company</th>
											<th>Class</th>
											<th>Period & fee</th>
											<th>Status</th>
											<th>Contract Starting Date</th>
											<th>Contract Expiration_date</th>
										</tr>
										


									</thead>
									<tbody>
									<?php foreach($trainees as $row){ ?>
										<tr>
							            <?php if($row->status == 'contract_completed'){ ?>
											<th><font color = " "><?= $row->traineeId; ?></th>
											<th><font color = " "><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = " "><?=$row->sex; ?></th>
											<th><font color = " "><?= $row->age; ?></th>
											<th><font color = " "><?=$row->phone; ?></th>
											<th><font color = " "><?=$row->email; ?></th>
											<th><font color = " "><?=$row->city; ?></th>
											<th><font color = " "><?=$row->company; ?></th>
											<th><font color = " "><?=$row->class; ?></th>
											<th><font color = " "><?=$row->schedule_fee; ?></th>
											<th><font color = "red"><?=$row->status; ?></th>
											<th><font color = " "><?=$row->starting_date; ?></th>
											<th><font color = " "><?=$row->expiration_date; ?></th>
											<?php } ?>
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>

								</div>
					</div>
	</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#view_trainees').DataTable();
									
			});
	</script>
</div>
</body>
</html>