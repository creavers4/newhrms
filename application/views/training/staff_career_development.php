<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
  <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3> <i class="icon-edit-sign"></i>  <i class="fa fa-desktop" aria-hidden="true"></i>
  Staff Career Development  <h3>
              </div>
              <div class="panel-body">
<script type="text/javascript" src="http//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http//ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http//ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
    rel="stylesheet" type="text/css" />


<?php echo validation_errors();?>
<?php echo form_open_multipart('controller_staff_career_development/staff_career_development'); ?>   
<div class="col-xs-8">
    <label for="trainerId">Staff ID  </label>
    <input type="text" class="form-control " id='staff_id' name="staff_id" value="<?php echo set_value('staff_id') ?>">
    <br>
    </div>
  <div class="col-xs-6">
    <label for="fName">First name  </label>
    <input type="text" class="form-control" name="fName" value="<?php echo set_value('fName') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="lName">Last name  </label>
    <input type="text" class="form-control" name="lName" value="<?php echo set_value('lName') ?>">
    <br>
    </div>
    
          <div class="col-xs-6">

    <label for="username">Phone  </label>
    <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone') ?>">
    <br>
    </div>
        <div class="col-xs-6">

    <label for="username">Email  </label>
    <input type="text" class="form-control" placeholder="eg. contact@email.com" name="email" value="<?php echo set_value('email') ?>">
    <br>
    </div>

    <div class="col-xs-6">

    <label for="username">Department  </label>
    <input type="text" class="form-control" name="department" value="<?php echo set_value('department') ?>">
    <br>
    </div>
    
    <div class="col-xs-6">

        <label for="sel1">Class</label>
        

          <select class="form-control" name="class" value="<?php echo set_value('class'); ?>" >
          <option value='default'>-- Select Class --</option>
          <?php foreach($trainer as $row1){ ?>
          <option value='<?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?>'><?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?></option>
          <?php } ?>
       
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

        <label for="sel1">Class Period & Fee</label>
        

          <select class="form-control" name="schedule_fee" value="<?php echo set_value('schedule_fee'); ?>" >
          <option value='default'>-- Select class Period & fee --</option>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->training; ?>: <?= $row->schedule; ?>: <?= $row->fee; ?>'><?= $row->training; ?>: <?= $row->schedule; ?>: <?= $row->fee; ?></option>
          <?php } ?>
       
          </select>
          
          <br>
    </div>
    
    <br>
    </div>
  <center><input type='submit' name='submit' value='Sign Up'></center>
  <br>
  </div>

<?php echo form_close(); ?>
</div>
</div>
<?php $this->load->view('layout/footer')?>
              </div>
              
              </div>
  
  <script>
    $(document).ready(function(){
      $('#tableemployess').DataTable();
                  
      });
              </script>
</div>
</body>
</html>