<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of trainees</h3>
							
						</div>
								<div class="panel-body">
								
											<?php echo validation_errors();?>
											<?php echo form_open_multipart('controller_trainee_registeration/update_status'); ?>
											<h4><a href="controller_training">Return</a></h4>
											<style>
											input[type="submit"] {
											background-color :red; 
											color: white;
											}
											</style>
											<input type='submit' name='unavailable' value="Terminate Expired Trainees">
											  <?php echo form_close(); ?>
											  <br><br>

								<table class="table table-striped table-hover" id="view_trainees">
									<thead>
										<tr>
											<th>Trainee Id</th>
											<th>Name</th>
											<th>Sex</th>
											<th>Age</th>
											<th>Phone</th>
											<th>Email</th>
											<th>City</th>
											<th>Company</th>
											<th>Class</th>
											<th>Period & fee</th>
											<th>Status</th>
											<th>Contract Starting Date</th>
											<th>Contract Expiration_date</th>
										</tr>
										


									</thead>
									<tbody>
									<?php foreach($trainees as $row){ ?>
										<tr>
										<?php if($row->status == 'on_contract'){ ?>
											<th><font color = "green"><?= $row->traineeId; ?></th>
											<th><font color = "green"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "green"><?=$row->sex; ?></th>
											<th><font color = "green"><?= $row->age; ?></th>
											<th><font color = "green"><?=$row->phone; ?></th>
											<th><font color = "green"><?=$row->email; ?></th>
											<th><font color = "green"><?=$row->city; ?></th>
											<th><font color = "green"><?=$row->company; ?></th>
											<th><font color = "green"><?=$row->class; ?></th>
											<th><font color = "green"><?=$row->schedule_fee; ?></th>
											<th><font color = "green"><?=$row->status; ?></th>
											<th><font color = "green"><?=$row->starting_date; ?></th>
											<th><font color = "green"><?=$row->expiration_date; ?></th>
							            <?php } ?>
							            <?php if($row->status == 'contract_completed'){ ?>
											<th><font color = "red"><?= $row->traineeId; ?></th>
											<th><font color = "red"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "red"><?=$row->sex; ?></th>
											<th><font color = "red"><?= $row->age; ?></th>
											<th><font color = "red"><?=$row->phone; ?></th>
											<th><font color = "red"><?=$row->email; ?></th>
											<th><font color = "red"><?=$row->city; ?></th>
											<th><font color = "red"><?=$row->company; ?></th>
											<th><font color = "red"><?=$row->class; ?></th>
											<th><font color = "red"><?=$row->schedule_fee; ?></th>
											<th><font color = "red"><?=$row->status; ?></th>
											<th><font color = "red"><?=$row->starting_date; ?></th>
											<th><font color = "red"><?=$row->expiration_date; ?></th>
											<?php } ?>
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>

								</div>
					</div>
	</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#view_trainees').DataTable();
									
			});
	</script>
</div>
</body>
</html>