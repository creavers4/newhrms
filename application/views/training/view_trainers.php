<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="fluid-container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
						<h3><i class="fa fa-desktop" aria-hidden="true"></i> List of trainers</h3>
							
						</div>
								<div class="panel-body">
    <h4><a href="controller_training">Return</a></h4>
								<?php echo validation_errors();?>
											<?php echo form_open_multipart('controller_trainer_registeration/update_status'); ?>
											<style>
											input[type="submit"] {
											background-color :red; 
											color: white;
											}
											</style>
											<input type='submit' name='unavailable' value="Terminate Expired Trainers">
											  <?php echo form_close(); ?>
											  <br><br>
								<table class="table table-striped table-hover" id="view_trainers">
									<thead>
										<tr>
											<th>Trainer Id</th>
											<th>Name</th>
											<th>Phone</th>
											<th>Email</th>
											<th>Training</th>
											<th>Experience</th>
											<th>Specialized Field</th>
											<th>Period</th>
											<th>Salary</th>
											<th>Status</th>
											<th>Contrcat Starting Date</th>
											<th>Contract Expiration Date</th>
										</tr>
										


									</thead>
									<tbody>
									<?php foreach($trainers as $row){ ?>
										<tr>
										<?php if($row->status == 'on_contract'){ ?>
										
											<th><font color = "green"><?= $row->trainerId; ?></th>
											<th><font color = "green"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "green"><?=$row->phone; ?></th>
											<th><font color = "green"><?=$row->email; ?></th>
											<th><font color = "green"><?=$row->training; ?></th>
											<th><font color = "green"><?=$row->experience; ?></th>
											<th><font color = "green"><?=$row->specialized_field; ?></th>
											<th><font color = "green"><?=$row->specific_stay; ?></th>
											<th><font color = "green"><?=$row->salary; ?></th>
											<th><font color = "green"><?=$row->status; ?></th>
											<th><font color = "green"><?=$row->starting_date; ?></th>
											<th><font color = "green"><?=$row->expiration_date; ?></th>

											<?php } ?>
											<?php if($row->status == 'contract_completed'){ ?>
										
											<th><font color = "grey"><?= $row->trainerId; ?></th>
											<th><font color = "grey"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "grey"><?=$row->phone; ?></th>
											<th><font color = "grey"><?=$row->email; ?></th>
											<th><font color = "grey"><?=$row->training; ?></th>
											<th><font color = "grey"><?=$row->experience; ?></th>
											<th><font color = "grey"><?=$row->specialized_field; ?></th>
											<th><font color = "grey"><?=$row->specific_stay; ?></th>
											<th><font color = "grey"><?=$row->salary; ?></th>
											<th><font color = "grey"><?=$row->status; ?></th>
											<th><font color = "grey"><?=$row->starting_date; ?></th>
											<th><font color = "grey"><?=$row->expiration_date; ?></th>
											<?php } ?>
											<?php if($row->status == 'available'){ ?>
										
											<th><font color = "blue"><?= $row->trainerId; ?></th>
											<th><font color = "blue"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "blue"><?=$row->phone; ?></th>
											<th><font color = "blue"><?=$row->email; ?></th>
											<th><font color = "blue"><?=$row->training; ?></th>
											<th><font color = "blue"><?=$row->experience; ?></th>
											<th><font color = "blue"><?=$row->specialized_field; ?></th>
											<th><font color = "blue"><?=$row->specific_stay; ?></th>
											<th><font color = "blue"><?=$row->salary; ?></th>
											<th><font color = "blue"><?=$row->status; ?></th>
											<th><font color = "blue"><?=$row->starting_date; ?></th>
											<th><font color = "blue"><?=$row->expiration_date; ?></th>
											<?php } ?>
											<?php if($row->status == 'unavailable'){ ?>
										
											<th><font color = "red"><?= $row->trainerId; ?></th>
											<th><font color = "red"><?= $row->fname." ".$row->lname; ?></th>
											<th><font color = "red"><?=$row->phone; ?></th>
											<th><font color = "red"><?=$row->email; ?></th>
											<th><font color = "red"><?=$row->training; ?></th>
											<th><font color = "red"><?=$row->experience; ?></th>
											<th><font color = "red"><?=$row->specialized_field; ?></th>
											<th><font color = "red"><?=$row->specific_stay; ?></th>
											<th><font color = "red"><?=$row->salary; ?></th>
											<th><font color = "red"><?=$row->status; ?></th>
											<th><font color = "red"><?=$row->starting_date; ?></th>
											<th><font color = "red"><?=$row->expiration_date; ?></th>
											<?php } ?>
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>
								</div>
					</div>
	</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#view_trainers').DataTable();
									
			});
	</script>
</div>
</body>
</html>