<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
  <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3> <i class="icon-edit-sign"></i>  <i class="fa fa-desktop" aria-hidden="true"></i> Update Student<h3>
              </div>
              <div class="panel-body">

<?php echo validation_errors();?>
<?php echo form_open_multipart('controller_trainees_update/update_trainees'); ?>
<div class="col-xs-6">
        <label for="sel1">Select Student</label>
          <select class="form-control" name="select_trainees" value="<?php echo set_value('select_trainees'); ?>" >
          <option value="none">-- Select Student --</option>
          <?php foreach($trainees_update as $row){ ?>
          <option value='<?= $row->traineeId; ?>'><?= $row->fname; ?> <?= $row->lname; ?></option>
          <?php } ?>
          </select>
          <br>
          </div>
          <br><br><br><br>
          <hr>

          <div class="col-xs-8">
  <p id="date_filter">
    <span id="date-label-from" class="date-label">Contract Starting Date: </span><input class="date_range_filter date" type="date" name="starting_date" />
  <span id="date-label-from" class="date-label">Contract Expiry Date: </span><input class="date_range_filter date" type="date" name="expiration_date" />
</p>
  <script>
    $(document).ready(function() {
$(function() {
  var oTable = $('#datatable').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });




  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);
} );  
  </script>

  

</div>

          <div class="col-xs-6">

    <label for="username">Phone  </label>
    <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone') ?>">
    <br>
    </div>
        <div class="col-xs-6">

    <label for="username">Email  </label>
    <input type="text" class="form-control" placeholder="eg. contact@email.com" name="email" value="<?php echo set_value('email') ?>">
    <br>
    </div>
    
  
    
        
        <div class="col-xs-6">

    <label for="username">City  </label>
    <select class="form-control" name="city" value="<?php echo set_value('city'); ?>" >
          <option value="none">-- Select City --</option>
          <option value="Addis Ababa">Addis Ababa</option>
          <option value="Other">Other</option>
       
          </select>
    <br>
    </div>
        <div class="col-xs-6">

    <label for="lName">Company  </label>
    <input type="text" class="form-control" name="company" value="<?php echo set_value('company') ?>">
    <br>
    </div>
    
    <div class="col-xs-6">

        <label for="sel1">Class</label>
        

          <select class="form-control" name="class" value="<?php echo set_value('class'); ?>" >
          <option value='none'>-- Select Class --</option>
          <?php foreach($trainer as $row1){ ?>
          <option value='<?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?>'><?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?></option>
          <?php } ?>
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

        <label for="sel1">Class Period & Fee</label>
        

          <select class="form-control" name="schedule_fee" value="<?php echo set_value('schedule_fee'); ?>" >
          <option value='none'>-- Select class Period & fee --</option>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->training; ?>: <?= $row->specify_schedule; ?>: <?= $row->fee; ?>'><?= $row->training; ?>: <?= $row->specify_schedule; ?>: <?= $row->fee; ?></option>
          <?php } ?>
       
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

    <label for="username">Status</label>
    <select class="form-control" name="status" value="<?php echo set_value('status'); ?>" >
          <option value="none">-- Select Trainer Status --</option>
          <option value="on_contract">On Contract</option>
          <option value="contract_completed">Contract Completed</option>
       
          </select>
    <br>
    </div>
      
    </div>
    <br>
  <center><input type='submit' name='submit' value='Update'> <br>
    <h4><a href="controller_training">Return</a></h4>
</center>
  <br>
  </div>

<?php echo form_close(); ?>

</div>
</div>
<?php $this->load->view('layout/footer')?>
              </div>
              
              </div>
  
  <script>
    $(document).ready(function(){
      $('#tableemployess').DataTable();
                  
      });
              </script>
</div>
</body>
</html>