<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('layout/header')?>
<body>
<?php $this->load->view('layout/navigation')?>
<?php $this->load->view('layout/sidebar');?>
 
       <hr>
        <div class="row">
        <div class="col-md-8 sidebar">
             <div class="panel panel-default">
                <div class="panel-heading">
                 <h3> <i class="fa fa-user"></i> : Change Password </h3> 
                    </div>
                    <div class="panel-body" width="100px">
            
                            <?= validation_errors() ?>
                                <hr>
                            <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
                            <div class="col-md-12">
                            <?php echo form_open_multipart('employee_info/change_password') ?>
							<div class="col-md-2">
							</div>
                            <div class="col-md-8">
                            <div class="form-group">
                                    <label for="Department">Old  Password : </label>
                                    <input type="password" class="form-control"placeholder ="Enter old  password" name="oldpassword" value="<?php echo set_value('oldpassword') ?>">
                                </div>
                                 <div class="form-group">
                                    <label for="Department">New  Password : </label>
                                    <input type="password" class="form-control"placeholder ="Enter old  password" name="password" value="<?php echo set_value('password') ?>">
                                </div>
                                 <div class="form-group">
                                    <label for="Department">Confirm New Password : </label>
                                    <input type="password" class="form-control"placeholder ="Enter old  password" name="confirmPassword" value="<?php echo set_value('confirmPassword') ?>">
                                </div>
                                </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="form-group">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Change</button>
                                    </div>
                                <div class="col-md-3">
                                    <?=  anchor('employee_info/employee_account','Cancel',['class'=>'btn  btn-default']) ?>
                                </div>
                                </div>
                            <?= form_close() ?>
                            </div>
                            <div class="col-md-3"></div>    
                        </div>  
            </div>  
            </div>
           <hr>
           <?php $this->load->view('layout/footer')?>
</body>
</html>