<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('layout/header')?>
<body>
<?php $this->load->view('layout/navigation')?>
    <div class="container">
       <hr>
        <div class="row">
             <div class="panel panel-default">
                <div class="panel-heading">
                 <h3> <i class="fa fa-user"></i> : Creat New Employee <?php echo anchor('employee_info/view_employee','View  Employee',['class'=>'btn btn-primary btn-xs']) ?> </h3> 
                    </div>
                    <div class="panel-body" width="100px">
            
                            <?= validation_errors() ?>
                                <hr>
                            <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
                            <div class="col-md-12">
                            <?php echo form_open_multipart('employee_info/create') ?>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label for="username">Enroll Number : </label>
                                    <input type="text" class="form-control" name="enroll_number" value="<?php echo set_value('enroll_number') ?>">
                                </div>
                            <div class="form-group">
                                    <label for="username">First Name : <?= $this->session->flashdata('error') ?></label>
                                    <input type="text" class="form-control" name="fname" value="<?php echo set_value('fname') ?>">
                                </div>

								<div class="form-group">
                                    <label for="username">Last Name : </label>
                                    <input type="text" class="form-control" name="lname" value="<?php echo set_value('lname') ?>">
                                </div>
								<div class="form-group">
                                    <label for="username">Sex: </label>
                                    
                                     <label class="radio-inline">Female</label>&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "Female"; ?>" checked="checked" />
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="radio-inline">Male</label>&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "Male"; ?>" />
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
		<div class="form-group">
         <label for="username">Birth Date: </label>
         <select name="day">
		  <option value="">-  Day -</option>
          <?php 
         for($i=1;$i<=31;$i++){      ?>
		  <option value="<?php echo $i ?>" ><?php echo $i ?></option>
          <?php  }?>
    	</select>
	
   <select name="month">
		  <option value="">-  Month -</option>
		  <option value="01" >January</option>
		  <option value="02" >February</option>
  		  <option value="03" >March</option>
		  <option value="04" >April</option>
		  <option value="05" >May</option>
		  <option value="06" >June</option>
		  <option value="07" >July</option>
		  <option value="08" >August</option>
		  <option value="09" >September</option>
		  <option value="10" >October</option>
		  <option value="11" >November</option>
		  <option value="12" >December</option>
    	</select>
  <select name="year">
		  <option value="">-  year -</option>
          <?php 
         for($i=1950;$i<=2017;$i++){      ?>
		  <option value="<?php echo $i ?>" ><?php echo $i ?></option>
          <?php  }?>
    	</select>
		 <?php foreach($select_dep_names as $select_dep_name): ?>
		<script>
		$(document).ready(function(){
			$('#department').on('change', function() {
  				var value = $(this).val();
				  console.log(value);
  				$('#job').html("  <option> <?php  foreach($select_jobe_names as $select_jobe_name){
							
                            echo $select_jobe_name->dep_name; } 
                              
							  ?> </option>");
			});


		}); 

		
</script>
<?php endforeach;?>
                                </div>
								<div class="form-group">
								<label for="username">Nationality: </label>
<!-- country codes (ISO 3166) and Dial codes. -->
<select name="countryName" id="" class="form-control" >
	<option value="">-  Nationality -</option>
    <option data-countryCode="ET" value="Ethiopia">Ethiopia</option>
	<optgroup label="Other countries">
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antartica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo">Congo, the Democratic Republic of the</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
    <option value="Croatia">Croatia (Hrvatska)</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="East Timor">East Timor</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia" selected>Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="France Metropolitan">France, Metropolitan</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
    <option value="Holy See">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran (Islamic Republic of)</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
    <option value="Korea">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon" >Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macau">Macau</option>
    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia">Micronesia, Federated States of</option>
    <option value="Moldova">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russia">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
    <option value="Saint LUCIA">Saint LUCIA</option>
    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia (Slovak Republic)</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
    <option value="Span">Spain</option>
    <option value="SriLanka">Sri Lanka</option>
    <option value="St. Helena">St. Helena</option>
    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syria">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Vietnam">Viet Nam</option>
    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Yugoslavia">Yugoslavia</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>
	</optgroup>
</select>
                                </div>
								<div class="form-group">
                                    <label for="username">Phone Number: </label>
                                    <input type="text" class="form-control" name="phoneNumber" value="<?php echo set_value('phoneNumber') ?>">
                                </div>
								  <div class="form-group">
								<label for="username">Department: </label>
							<select id="department" class="form-control" name="depName" value = "<?php echo set_value('depName'); ?>">
							 <option value="">-  Select department -</option>
                            <?php foreach($select_dep_names as $select_dep_name) :?>
							
                            <option><?php  echo $select_dep_name->dep_name; ?> </option>
                             <?php endforeach;?>
                             </select>
                            
                                </div>
                                
								 <div class="form-group">
								<label for="username">Employee Type: </label>
							<select id="employeeType" class="form-control" name="employeeType" value = "<?php echo set_value('employeeType'); ?>">
							 <option value="">-  Select Employee Type -</option>
                            <option>Permanet </option>
                             <option>Project </option>
                             </select>
                            
                                </div>
                                </div>
                                <div class="col-md-6">
                      
								<div class="form-group">
								<label for="username">Jobe: </label>
							<select id="job1" class="form-control" name="jobeTitle" value= "<?php echo set_value('jobeTitle'); ?>">
							 <option value="">-  Select Jobe -</option>
                            <?php  foreach($select_jobe_titles as $select_jobe_title) :?>
							
                            <option><?php  echo $select_jobe_title->jobe_title; ?> </option>
                             <?php endforeach;?>
                             </select>
                            
                                </div>
								<div class="form-group">
								<label for="username">Educational Level: </label>
                <select class="form-control" name="eduLevel" value ="<?php echo set_value('eduLevel'); ?>">
                <option>Select...</option>
                <option>Doctor</option>
                <option>Master</option>
                <option>Degree</option>
                <option>Diploma</option>
                <option>Level V</option>
                <option>Level IV</option>
                <option>Level III</option>
                <option>Level II</option>
                <option>Level I</option>
            </select>
                                </div>
								<div class="form-group">
                                    <label for="password">upload Document :</label>
								<input type="file" name="userfile">
                                </div>
								<div class="form-group">
                                    <label for="username">User Name : </label>
                                    <input type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>">
                                </div>
								 <div class="form-group">
								<label for="username">Employee Role: </label>
							<select class="form-control" name="role" value = "<?php echo set_value('role'); ?>">
							 <option value="">-  Select role -</option>
							
                            <option>admin</option>
							<option>employee</option>
                             </select>
                            
                                </div>
								<div class="form-group">
                                    <label for="password">password : </label>
                                    <input type="password" class="form-control" name="password" id="pass1" onkeyup="pass2null(); return false; required" value="<?php echo set_value('password') ?>">
                                </div>
								<div class="form-group">
                                    <label for="username">Confirm Password :<span id="confirmMessage" class="confirmMessage"></span> </label>
                                    <input type="password" class="form-control" name="confirmPassword"   id="pass2" onkeyup="checkPass(); return false;" value="<?php echo set_value('confirmPassword') ?>">
                                
							    </div>
                                </div>
                              
                                <div class="col-md-1">
                                </div>
                                <div class="form-group">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Register</button>
                                    </div>
                                <div class="col-md-3">
                                    <?=  anchor('employee_info/view_employee','Cancel',['class'=>'btn  btn-default']) ?>
                                </div>
                                </div>
								  </div>
                            <?= form_close() ?>
                            </div>
                            <div class="col-md-3"></div>    
                        </div>  
            </div>  
            </div>
           <hr>
           <?php $this->load->view('layout/footer')?>


<script type="text/javascript">
  document.getElementById("field_terms").setCustomValidity("Please indicate that you accept the Terms and Conditions");
</script>
<script>
function pass2null(){
	var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    pass2.style.borderColor = "red";
}
function checkPass()
{
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    var message = document.getElementById('confirmMessage');
    if(pass1.value == pass2.value){
        pass2.style.borderColor = "green";
        message.style.color = "green";
        message.innerHTML = "Passwords Match"
    }else{
        pass2.style.borderColor = "red";
        message.style.color = "red";
        message.innerHTML = "Passwords Do Not Match!"
    }
}

</script>

</body>
</html>
	<?php  //foreach($select_dep_names as $select_dep_name){
							
                         //   echo $select_dep_name->dep_name; } ?>