<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
						
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i> 
							
							<?php foreach ($department_name as $department_names) :?>
						<?php $name = $department_names->dep_name;
						if($name == "Maneger")
						echo "List Of All Employee";
						if($name);
						else{
                     echo $name;
						}
						
						break;
						?>

						<?php endforeach;?>
					<?php echo $name." "."Department" ?>
					<?php foreach ($employees as $employee) :?>
								
							 <?php echo anchor('employee_info','Add New Employee',['class'=>'btn btn-primary btn-xs']) ?> <h4>
							 <?php if($employee->role_level=="department head"):?>
							  <?php echo anchor('employee_info/view_best_worker_of_my_employee','Best Worker Of My Department',['class'=>'btn btn-primary btn-xs']) ?>  <?php echo $no_bestworker?><h4>	 
							<?php endif;?>
							</div>
							<?php break; endforeach;?>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th>Full Name</th>
										<th>Phone Number</th>
										<th>education</th>
										<th>salary</th>
										<?php   $super_role =   $role_level->role_level; ?>
												<?php if($super_role == 'superadmin'):?>
										<th>Action</th>
										<?php endif;?>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($employees as $employee) :?>
												<tr>
												<th><?php  echo anchor('employee_info/employeee_profile/'.$employee->emp_id,$employee->first_name."  ".$employee->last_name);?></th>
												
												<th><?php  echo $employee->phone_number;?></th>
												<th><?php  echo anchor('employee_info/view_document/'.$employee->emp_id,$employee->edu_level);?></th>
												

												<th><?php  echo anchor('payrolls/employee_slip/'.$employee->emp_id,$employee->basic_sallary);?></th>
												<?php   $super_role =   $role_level->role_level; ?>
												<?php if($super_role == 'superadmin'):?>
												
												<th>
												<?php if($employee->role_level == 'superadmin'):?>
											<?php	echo "This is Admin";?>
											
												<?php else:?>
											<?php	if($employee->emp_status==1):?>
								<?php echo  anchor('employee_info/disable_emp/'.$employee->emp_id,'Disable ',['class'=>'btn btn-danger btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Disabled This user ? \')'
								])  ?>
								<?php endif;?>
								<?php	if($employee->emp_status==0):?>
                             <?php  echo  anchor('employee_info/active_employee/'.$employee->emp_id,'Active ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Active This user ? \')'
								])  ?>
							<?php endif;?>
	
								<?php echo  anchor('creat_new_admin/Edit_employee/'.$employee->emp_id,'Edit ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Edit This user ? \')'
								])  ?>
								</th>
								<?php endif;?>
								<?php endif;?>
								
							                   	</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							<?php $this->load->view('layout/footer')?>
							</div>
							
	</div>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
    </script>
</div>
</body>
</html>