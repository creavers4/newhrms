<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>

<?php $this->load->view('layout/sidebar')?>
	<div class="row">
<div class="col-md-8 sidebar">
<?php foreach($profies as $profile):?>
<div class="panel panel-default">
						<div class="panel-heading">
							<h2> <i class="icon-edit-b"></i> <i class="fa fa-fw fa-book"></i>Inbox from HR<h2>
							</div>
							<div class="panel-body">
							<h4>Select And Approve Message</h4>
							<br>
							<?php echo form_open_multipart('controller_message/update_approval') ?>
							
    <div class="radio">
	
    <div class="col-xs-3">
        <label for="sel1">Message ID </label>
          <select class="form-control" name="message_i" value="<?php echo set_value('message_i'); ?>" >
          <option value="none">-- Select ID --</option>
          <?php foreach($message as $row){ ?>
          <option value='<?= $row->message_id; ?>'><?= $row->message_id; ?></option>
          <?php } ?>
          </select>
          <br>
          </div>
        
<div class="col-xs-3">
  <label for="comment">Message</label>
  <textarea class="form-control" id="message" name="message" value="<?php echo set_value('message') ?>"></textarea>
  </div>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Send" value="Send">
		</div>
		
  
		<?php echo form_close(); ?>

		<br>
							<table class="table table-striped table-hover" id="view_trainees">
									<thead>
										<tr>
										<th>Date</th>
											<th><font color="red">Message ID </font></th>
											<th>Sender ID</th>
											<th>Reviever ID</th>
											<th>Subject</th>
											<th>Message</th>
											<th>Reply</th>
										</tr>
										


									</thead>
									<tbody>
									<?php foreach($view_message as $row){ ?>
										<tr>
										<th><?= $row->date; ?></th>
										<th><font color="red"><?= $row->message_id; ?></font></th>
											<th><?= $row->sender_id; ?></th>
											<th><?= $row->reciver_id ?></th>
											<th><?=$row->subject; ?></th>
											<th><?= $row->message; ?></th>
											<th><?= $row->approval; ?></th>
							            </tr> 
							            <?php } ?>
							        </tbody>
								</table>

						
							 <?php if($profile->employee_type == "project"){?>

									<div class="col-md-5">
									<h1> Assigned Task </h1>
									
									
									
						
  <?php echo form_open_multipart('task_evaluation/send_my_task_to_evaluator') ?>

						<?php foreach($view_assigned_task as $view_my_tasks):?>
    
        <div class="checkbox">
          <label>
            <input type="checkbox" name="checkbox" value="<?php echo $view_my_tasks->emp_id ?>">
            <span class="cr"></i></span>
           <?php   echo  $view_my_tasks->task_title; ?> <br>
		   <?php   echo  $view_my_tasks->created_date." "."by".$view_my_tasks->assigne_by; ?> <br>
          </label>
          </div>
						
							 <?php endforeach;?>
							  <div class="col-md-3">
                                    <button type="submit" class="btn btn-success">Send</button>
                                    </div>
                                <div class="col-md-3">
                                    <?=  anchor('employee_info/employee_account','Cancel',['class'=>'btn  btn-default']) ?>
                                </div>
								
							<?php }?>
							   <?= form_close() ?>
							   <?php break;
							    endforeach;?>
									</div>
									
									</div>
									<?php $this->load->view('layout/footer')?>
							</div>
							
							</div>
							</div>
	
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>