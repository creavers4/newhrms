<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3> <i class="icon-edit-sign"></i>  <i class="fa fa-user"></i>  Administretor <?php echo anchor('creat_new_admin','Add New Administretor',['class'=>'btn btn-primary btn-xs']) ?> <h3>
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th>Employee Id</th>
										<th>Employee First Name</th>
										<th>Employee Last Name
										<th>Employee role</th>
										<th>Employee status</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($view_administretors as $view_administretor) :?>
												<tr>
												<th> <?php echo $view_administretor->user_id ;?></th>
												<th><?php  echo $view_administretor->first_name;?></th>
												<th><?php  echo $view_administretor->last_name;?></th>
												<th><?php  echo $view_administretor->role;?></th>
												<th>
								<?php if($this->session->userdata('role') == 'admin') : echo  anchor('creat_new_admin/disable_admin/'.$view_administretor->user_id,'Disabled ',['class'=>'btn btn-danger btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Disabled This user ? \')'
								])  ?>
								<?php endif;?>

                             <?php if($this->session->userdata('role') == 'admin') : echo  anchor('creat_new_admin/active_admin/'.$view_administretor->user_id,'Active ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Disabled This user ? \')'
								])  ?>
								<?php endif;?>

	
								<?php if($this->session->userdata('role') == 'admin') : echo  anchor('creat_new_admin/Edit_employee/'.$view_administretor->user_id,'Edit ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want Edit This user ? \')'
								])  ?>
								<?php endif;?>
								
								
								

												</th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>