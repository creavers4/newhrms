<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i>  Department <?php echo anchor('creat_new_admin/create_new_department','Add New Department',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th>Department Name</th>
										<th>Date</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($select_dep_names as $select_dep_name) :?>
												<tr>
												<th>
												<?php if($select_dep_name->dep_name == "Maneger"){
													 echo anchor('employee_info/view_employee_for_admin/'.$select_dep_name->dep_id,'All Employee');
												}else{
													echo anchor('employee_info/view_employee_for_admin/'.$select_dep_name->dep_id,$select_dep_name->dep_name);
												}?>
												</th>
												<th>
												
												<?php  echo $select_dep_name->created_date;?>
												
												
												</th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>