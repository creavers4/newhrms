<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header'); ?>
<body>
<?php $this->load->view('layout/navigation'); ?>
<div class="container">
	<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i>  Jobs <?php echo anchor('creat_new_admin/create_new_jobe','Add New Job',['class'=>'btn btn-primary btn-xs']) ?> <h4>	 
							</div>
							<div class="panel-body">
							
									<table class="table table-striped table-hover" id="tableemployess">
										<thead>
									<tr>
										<th>Job Title</th>
										<th>Jobe Description</th>
										<th>Basic salary</th>
										<th>Tax</th>
										<th>Action</th>
									</tr>
										</thead>
										<tbody>
											<?php foreach ($select_jobe_names as $select_jobe_name) :?>
												<tr>
												<th><?php  echo $select_jobe_name->jobe_title;?></th>
												<th><?php  echo $select_jobe_name->jobe_description;?></th>
												<th><?php  echo $select_jobe_name->basic_sallary;?></th>
												<th><?php  echo $select_jobe_name->tax;?></th>
												
												<th>  
								<?php if($this->session->userdata('role') == 'admin') : echo  anchor('#'.$select_jobe_name->jobe_id,'Edit ',['class'=>'btn btn-primary btn-xs ',
									'onclick'=>'return confirm(\'Are You Sure You Want to Add Over Time This user ? \')'
								])  ?>
								<?php endif;?>
								
								
								

												</th>
												</tr> 
												<?php endforeach; ?>
												
										</tbody>
									</table>
									</div>
							</div>
							</div>
							
	</div>
	<?php $this->load->view('layout/footer')?>
	<script>
		$(document).ready(function(){
			$('#tableemployess').DataTable();
									
			});
							</script>
</div>
</body>
</html>