<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('layout/header')?>
<body>
<?php $this->load->view('layout/navigation')?>
    <div class="container">
       <hr>
        <div class="row">
             <div class="panel panel-default">
                <div class="panel-heading">
              <h4> <i class="icon-edit-sign"></i> <i class="fa fa-fw fa-compass"></i>  Jobs <?php echo anchor('creat_new_admin/select_all_jobe_info','View Job',['class'=>'btn btn-primary btn-xs']) ?> <h4>
                    </div>
                    <div class="panel-body" width="100px">
            
                            <?= validation_errors() ?>
                                <hr>
                            <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
                            <div class="col-md-12">
                            <?php echo form_open_multipart('creat_new_admin/create_new_jobe/') ?>
							<div class="col-md-2">
							</div>
                            <div class="col-md-8">
                            <div class="form-group">
                                    <label for="Jobe Name">Jobe  Form : </label>
                                    <div class="input-group">
							<div class="input-group-addon">Department</div>
							<select class="form-control" name="depName" <?php echo set_value('depName'); ?>>
                            <?php  foreach($select_dep_names as $select_dep_name) :?>
                            <option><?php  echo $select_dep_name->dep_name; ?> </option>
                             <?php endforeach;?>
                             </select>
                            </div>
                                    <input type="text" class="form-control"placeholder ="Enter jobe  Title" name="jobeTitle" value="<?php echo set_value('jobeTitle') ?>">
                                    <input type="text" class="form-control"placeholder ="Enter jobe  Description" name="jobeDiscription" value="<?php echo set_value('jobeDiscription') ?>">
                                     <input type="text" class="form-control"placeholder ="Enter Sallary  " name="basic_sallary" value="<?php echo set_value('basic_sallary') ?>">
                                </div>
                                </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="form-group">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    </div>
                                <div class="col-md-3">
                                    <?=  anchor('creat_new_admin/select_all_jobe_info','Cancel',['class'=>'btn  btn-default']) ?>
                                </div>
                                </div>
                            <?= form_close() ?>
                            </div>
                            <div class="col-md-3"></div>    
                        </div>  
            </div>  
            </div>
           <hr>
           <?php $this->load->view('layout/footer')?>
</body>
</html>