<?php 
	$emp_id 				= $employee->emp_id;
if($this->input->post('is_submitted'))
{
		$first_name	     	= set_value('firstName');
		$last_name			= set_value('lastName');
}else{

		$first_name 		= $employee->first_name;
		$last_name			= $employee->last_name;
	
}
	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('layout/header') ?>

	
	<body>
		<?php $this->load->view('layout/navigation')?>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>
								<i class="fa fa-fw fa-compass"></i>  Edit Employee
							</h4>
						</div>
						<div class="panel-body">
						<div><?= validation_errors()?></div>
						<?=  form_open_multipart('creat_new_admin/edit_employee/'.$emp_id,['class'=>'form-group']) ?>
							<div class="col-sm-4">
								<div class="input-group">
									<div class="input-group-addon">First Name</div>
									<input type="text" class="form-control" name="first_name" placeholder="Enter Name" value="<?= $first_name ?>">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="input-group">
									<div class="input-group-addon">Title</div>
									<input type="text" class="form-control" name="last_name" placeholder="Enter Product Title" value="<?= $last_name ?>">
								</div>
							</div>
							<div class="col-sm-12"><hr></div>
							<div class="col-sm-1">
								<div class="input-group">
									<input type="hidden" name="is_submitted" value="1">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</div>
							<div class="col-sm-1">
								<div class="input-group">
									
									<?php echo  anchor('employee_info/view_employee','Cancel',['class'=>'btn btn-danger']) ?>
								</div>
							</div>
							
						
						<?php echo form_close() ?>
						</div>
					</div>
				</div> 
				
			</div>
		
			
			
		</div>
		<hr>
			
			<?php $this->load->view('layout/footer')?>
	</body>
</html>
